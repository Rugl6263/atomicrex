import atomicrex

__doc__ = """This script tests if fit and relax flags are set correctly.  """

job = atomicrex.Job()
job.parse_input_file('main.xml')

fit_checks = {"total-energy": True,
              "C11": True,
              "C12": False,
              "lattice-parameter": True,
              "ca-ratio": False}

relax_checks = {"lattice-parameter": True,
                "ca-ratio": False}

for s_name, struct in job.structures.items():
    for p_name, setting in fit_checks.items():
        assert setting == struct.properties[p_name].fit_enabled

    for p_name, setting in relax_checks.items():
        assert setting == struct.properties[p_name].relax
