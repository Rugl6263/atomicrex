from __future__ import print_function, division
import atomicrex
import itertools
from ase import Atom, Atoms

job = atomicrex.Job()
job.parse_input_file('main.xml')
job.set_verbosity(2)
structure = job.add_library_structure('test-fcc',
                                      'fcc',
                                      {'alat': 4.032, 'type': 'Cu'})

s = structure.id
nat = structure.get_number_of_atoms()
cell = structure.cell
cell = structure.get_cell()

properties = []
properties.append('atomic-energy')
properties.append('total-energy')
properties.append('atomic-volume')
properties.append('total-volume')
properties.append('bulk-modulus')
for i, j in itertools.product(range(6), range(6)):
    if j < i:
        continue
    properties.append('C{}{}'.format(i+1, j+1))
for prop in properties:
    structure.modify_property(prop, 1.0, 0.10)

structure.set_target_forces([[0, 0, 1.]])
structure.compute_properties()
structure.print_properties()

conf = structure.get_atoms(job)

structure = job.add_library_structure('tst-sc',
                                      'sc',
                                      {'alat': 3.0, 'type': 'Cu'})
structure = job.add_library_structure('tst-bcc',
                                      'bcc',
                                      {'alat': 3.0, 'type': 'Cu'})
structure = job.add_library_structure('tst-hcp',
                                      'sc',
                                      {'alat': 3.0,
                                       'clat': 5.0,
                                       'type': 'Cu'})
structure = job.add_library_structure('tst-omega',
                                      'omega',
                                      {'alat': 2.5,
                                       'ca_ratio': 0.69,
                                       'type': 'Cu'})

conf = Atoms(cell=[[3.6, 0, 0],
                   [0, 3.6, 0],
                   [0, 0, 3.5]], pbc=True)
conf.append(Atom('Cu', [0.1, 0, 0]))
conf.append(Atom('Cu', [0, 0.5, 0.5]))
conf.append(Atom('Cu', [0.5, 0, 0.5]))
conf.append(Atom('Cu', [0.5, 0.5, 0]))
structure = job.add_ase_structure('tst-ase', conf)
structure.compute_energy(compute_forces=True)
energy = structure.get_potential_energy()
forces = structure.get_forces()
forces = structure.forces
pos = structure.positions
pos = structure.get_positions()
