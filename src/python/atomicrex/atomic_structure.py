from ._atomicrex import AtomicStructure
from collections import OrderedDict


def __AtomicStructure_get_atoms(self, job):
    """Returns the structure as an ASE atoms object.

    Parameters
    ----------
    job:
        atomicrex job object; this is used to translate the atomicrex atom
        types to element names as understood by ASE

    Returns
    -------
    ASE atoms object
        atomic structure
    """
    import ase
    conf = ase.Atoms(pbc=self.pbc)
    conf.set_cell(self.cell)
    for type, position in zip(self.types, self.positions):
        elem = job.types[type]
        conf.append(ase.Atom(elem, position))
    return conf
AtomicStructure.get_atoms = __AtomicStructure_get_atoms


# Provide access to properties of AtomicStructure in the form of a dictionary.
def __AtomicStructure_properties(self):
    """dict: Properties associated with structure.
    """
    dict_properties = OrderedDict()
    for prop in self._properties:
        dict_properties[prop.id] = prop
    return dict_properties
AtomicStructure.properties = property(__AtomicStructure_properties)


def __AtomicStructure_print_properties(self, show_all=False):
    """Print properties that are enabled for output and/or fitting.

    Parameters
    ----------
    show_all : bool
        set to True in order to show all properties regardless of their
        internal preference (as determined from output_enabled and
        fit_enabled)

    Notes
    -----
    Note that :py:meth:`.compute_properties` must be called first in order to
    update the stored values of the structure's properties.
    """
    for prop in self._properties:
        if not prop.output_enabled and not prop.fit_enabled and not show_all:
            continue
        prop.print_property()
AtomicStructure.print_properties = __AtomicStructure_print_properties


# Convenience function for adding/modifying a property
def __AtomicStructure_modify_property(self, id, target_value=None,
                                      relative_weight=1.0,
                                      fit_enabled=None,
                                      output_enabled=True):
    """Add and/or modify a property.

    Parameters
    ----------
    id: string
        property identifier
    target_value: double
        the target value [default: no target value]
    relative_weight: double
        relative weight of the property if it is included in the
        calculation of the objective function
    fit_enabled: bool
        calculation of the objective function
    output_enabled: bool
        set to True in order to include the property when printing the
        results
    """
    try:
        self.properties[id].output_enabled = output_enabled
    except:
        raise Exception('Error, unknown property: {}'.format(id))
    if target_value is not None:
        try:
            if fit_enabled is not None:
                self.properties[id].fit_enabled = fit_enabled
            else:
                self.properties[id].fit_enabled = True
            self.properties[id].target_value = target_value
        except:
            raise Exception('Failed setting target value'
                            ' for property: {}'.format(id))
        try:
            self.properties[id].relative_weight = relative_weight
        except:
            raise Exception('Failed setting relative weight'
                            'for property: {}'.format(id))
AtomicStructure.modify_property = __AtomicStructure_modify_property


# Convenience function for deactivating a property
def __AtomicStructure_deactivate_property(self, id,
                                          fit_enabled=False,
                                          output_enabled=False):
    """Deactivates a property in the structure, i.e. removes its
    contribution to the residual (objective function).

    Parameters
    ----------
    id: string
        property identifier
    fit_enabled: bool
        set to True in order to still include the property in the
        calculation of the objective function
    output_enabled: bool
        set to True in order to still include the property when
        printing the results
    """
    try:
        self.properties[id].output_enabled = output_enabled
        self.properties[id].fit_enabled = fit_enabled
    except:
        raise Exception('Error, failed deactivating property: {}'.format(id))
    return
AtomicStructure.deactivate_property = __AtomicStructure_deactivate_property


# Simplified interface to compute properties
def __AtomicStructure_compute_properties(self, is_fitting=False):
    """Computes all enabled properties of the structure.

    Parameters
    ----------
    is_fitting: bool
        Determines whether this DOF should be relaxed
        (usually the desired behavior depends on the program phase).
    """

    # Compute standard properties
    self._compute_properties(is_fitting)

    # Update all remaining properties
    for prop in self._properties:
        prop.compute()
AtomicStructure.compute_properties = __AtomicStructure_compute_properties


def __AtomicStructure_compute_energy(self, compute_forces=False,
                                     is_fitting=False,
                                     suppress_relaxation=False):
    """Computes the total energy and optionally the forces for this
    structure.

    The function returns a scalar representing the energy of the
    structure. If the forces are computed they are stored in the
    structure object.

    Parameters
    ----------
    compute_forces: bool
        Turns on the computation of forces.
    is_fitting: bool
        Determines whether this DOF should be relaxed (usually the
        desired behavior depends on the program phase).
    suppress_relaxation: bool
        If True structure relaxation will be suppressed.
    """
    return self._compute_energy(compute_forces, is_fitting,
                                suppress_relaxation)
AtomicStructure.compute_energy = __AtomicStructure_compute_energy


def __AtomicStructure_set_target_forces(self, target_forces):
    """Set the target forces for a structure.

    Parameters
    ----------
    target_forces: Nx3 array of floats
        list of atom vectors representing the target forces
    """
    if len(target_forces) != self.get_number_of_atoms():
        raise Exception('Dimension of target_force vector does not match'
                        ' number of atoms in structure\n'
                        ' structure: {}'
                        ' input: {}'.format(self.get_number_of_atoms(),
                                            len(target_forces)))
    self.properties['atomic-forces'].set_target_values(target_forces)
AtomicStructure.set_target_forces = __AtomicStructure_set_target_forces


def __AtomicStructure_get_target_forces(self):
    """Get the target forces for a structure.

    Returns
    -------
    Nx3 array of floats
        the target forces
    """
    return self.properties['atomic-forces'].get_target_values()
AtomicStructure.get_target_forces = __AtomicStructure_get_target_forces
AtomicStructure.target_forces = property(__AtomicStructure_get_target_forces,
                                         __AtomicStructure_set_target_forces)


def __AtomicStructure_pbc(self):
    """
    list of three booleans: Periodic boundary conditions.
    """
    pbc = 3 * [False]
    for k in range(3):
        pbc[k] = self.has_pbc(k)
    return pbc
AtomicStructure.pbc = property(__AtomicStructure_pbc)


def __AtomicStructure_get_pbc(self):
    """
    Periodic boundary conditions.

    Returns
    -------
    list containing three booleans:
        the periodic boundary conditions
    """
    return self.pbc
AtomicStructure.get_pbc = __AtomicStructure_get_pbc
