from ._atomicrex import Job, residual_norm
import numpy as np
from collections import OrderedDict


def __Job_calculate_gradient(self, parameters=None, eps=1e-4):
    """Computes the gradient of the objective function with respect to
    the potential parameters using a central finite difference scheme.

    Parameters
    ----------
    parameters: list of floats
       the parameter set, at which the derivative is to be computed

    eps:
       step length for numerical differentiation
    """

    # store original parameter values
    params_original = self.get_potential_parameters()
    if parameters is not None:
        self.set_potential_parameters(parameters)
    params0 = self.get_potential_parameters()

    # compute gradient
    grad = []
    npar = len(params0)
    for i in range(npar):
        params = params0.copy()
        params[i] += eps
        chi_fwd = self.calculate_residual(params)
        params = params0.copy()
        params[i] -= eps
        chi_bwd = self.calculate_residual(params)
        grad.append(0.5 * (chi_fwd - chi_bwd) / eps)

    # reset parameters to original values
    self.set_potential_parameters(params_original)
    return grad
Job.calculate_gradient = __Job_calculate_gradient


def __Job_calculate_hessian(self, parameters=None, eps=1e-4):
    """Computes the matrix of second derivative of the objective function
    with respect to the potential parameters using a central finite
    difference scheme.

    Parameters
    ----------
    parameters: list of floats
       the parameter set, at which the derivative is to be computed

    eps:
       step length for numerical differentiation
    """

    # store original parameter values
    params_original = self.get_potential_parameters()
    if parameters is not None:
        self.set_potential_parameters(parameters)
    params0 = self.get_potential_parameters()

    # compute second derivative
    chi0 = self.calculate_residual()
    deriv = []
    npar = len(params0)
    for i in range(npar):
        row = []
        deriv.append(row)
        for j in range(npar):
            params = params0.copy()
            params[i] += eps
            params[j] += eps
            chi_fwd = self.calculate_residual(params)
            params = params0.copy()
            params[i] -= eps
            params[j] -= eps
            chi_bwd = self.calculate_residual(params)
            row.append((chi_fwd + chi_bwd - 2 * chi0) / eps)

    # reset parameters to original values
    self.set_potential_parameters(params_original)
    deriv = np.array(deriv)
    return 0.5 * (deriv + deriv.T)
Job.calculate_hessian = __Job_calculate_hessian


# Implementation of the Job.add_user_structure() method.
def __Job_add_ase_structure(self, structure_id, atoms,
                            structure_group=None):
    """Add a structure based on an ASE atoms object.

    Parameters
    ----------
    structure_id: string
        structure name; to be used later for accessing structure in
        dictionary

    atoms: ASE atoms object
        atomic configuration

    structure_group: reference to a structure group
        reference to structure group, to which this structure is
        to be assigned; the default (`None`) implies
        the root group

    """
    struct = self.create_user_structure(structure_id)

    # Convert simulation cell geometry and periodic boundary flags.
    struct.setup_simulation_cell(atoms.get_cell().T,
                                 (0, 0, 0),
                                 [bool(p) for p in atoms.get_pbc()])

    # Transfer atom list.
    struct.local_atom_count = len(atoms)
    struct.set_positions(atoms.get_positions())

     # Convert ASE chemical symbols to internal atom types.
    types = self.types
    try:
        struct.set_types([types.index(s) for s in
                          atoms.get_chemical_symbols()])
    except ValueError as e:
        raise ValueError("Structure import failed. No corresponding"
                         " atom type has been defined in fit job. %s" % e)

    # Add new structure to structure group.
    if structure_group is None:
        structure_group = self.root_group
    structure_group.registerSubObject(struct, True)

    return struct
Job.add_ase_structure = __Job_add_ase_structure


# Implementation of the Job.add_user_structure() method.
def __Job_add_library_structure(self, id, structure, params,
                                structure_group=None):
    """Add a structure from the library of predefined structures.

    Parameters
    ----------
    id: string
        structure name that will be used later to access the structure
        in the dictionary

    structure: string
        type of structure

        single element:
        `FCC`, `BCC`, `DIA`, `SC`, `HCP`, `DHCP`, `beta tin`, `omega`
        etc.

        multiple elements:
        `rocksalt`, `cesium chloride`, `zinc blende`, `D8a`, `L12`,
        `fluorite`, `C15`, `Bh`, `D2d`, `Ni17Th2`, `Th2Zn17`, `L10`,
        `wurtzite` etc.

    params: dictionary
        structural parameters including e.g,
        `alat`, `clat`, `ca_ratio`, `type`, `type_A`,
        `type_B` etc.

    structure_group: reference to a structure group
        reference to structure group, to which this structure is
        to be assigned; the default (`None`) implies
        the root group

    """

    # List of permissible lattice type identifiers
    fcc = ['A1', 'FCC', 'fcc', 'face-centered cubic', 'face-centred cubic']
    bcc = ['A2', 'BCC', 'bcc', 'body-centered cubic', 'body-centred cubic']
    diamond = ['A4', 'DIA', 'dia', 'diamond']
    sc = ['A4', 'SC', 'sc', 'simple cubic']
    unary_cubic = fcc + bcc + diamond + sc

    hcp = ['A3', 'HCP', 'hcp']
    dhcp = ['DHCP', 'dhcp']
    beta_tin = ['beta tin', 'beta Sn', 'A5']
    omega = ['omega']
    unary_hexa_rhombo_tetra = hcp + dhcp + omega + beta_tin

    rocksalt = ['B1', 'NaCl', 'rock salt']
    cesiumchloride = ['B2', 'CsCl', 'cesium chloride']
    zincblende = ['B3', 'zinc blende']
    D8a = ['D8a', 'Mn23Th6']
    L12 = ['L12', 'AuCu3']
    C1 = ['C1', 'CaF2', 'fluorite']
    C15 = ['C15', 'Cu2Mg', 'Fe2Y']
    binary_cubic = rocksalt + cesiumchloride + zincblende
    binary_cubic += D8a + L12 + C1 + C15

    Bh = ['Bh', 'WC', 'tungsten carbide']
    D2d = ['D2d']
    Ni17Th2 = ['Ni17Th2']
    Th2Zn17 = ['Th2Zn17']
    L10 = ['CuAu', 'L10']
    B4 = ['B4', 'wurtzite']
    binary_hexa_rhombo_tetra = Bh + D2d + Ni17Th2 + Th2Zn17 + L10 + B4

    # Set up structure
    if structure in fcc:
        struct = self.create_FCC_structure(id)
    elif structure in bcc:
        struct = self.create_BCC_structure(id)
    elif structure in diamond:
        struct = self.create_DIA_structure(id)
    elif structure in sc:
        struct = self.create_SC_structure(id)

    elif structure in hcp:
        struct = self.create_HCP_structure(id)
    elif structure in dhcp:
        struct = self.create_DHCP_structure(id)
    elif structure in omega:
        struct = self.create_OMG_structure(id)
    elif structure in beta_tin:
        struct = self.create_betaSn_structure(id)

    elif structure in rocksalt:
        struct = self.create_B1_structure(id)
    elif structure in cesiumchloride:
        struct = self.create_B2_structure(id)
    elif structure in zincblende:
        struct = self.create_B3_structure(id)

    elif structure in D8a:
        struct = self.create_D8a_structure(id)
    elif structure in L12:
        struct = self.create_L12_structure(id)
    elif structure in C1:
        struct = self.create_C1_structure(id)
    elif structure in C15:
        struct = self.create_C15_structure(id)

    elif structure in Bh:
        struct = self.create_Bh_structure(id)
    elif structure in D2d:
        struct = self.create_D2d_structure(id)
    elif structure in Ni17Th2:
        struct = self.create_Ni17Th2_structure(id)
    elif structure in Th2Zn17:
        struct = self.create_Th2Zn17_structure(id)
    elif structure in L10:
        struct = self.create_L10_structure(id)
    elif structure in B4:
        struct = self.create_B4_structure(id)

    else:
        print('Unknown structure: {}'.format(structure))
        return

    # Set atom types
    if structure in unary_cubic + unary_hexa_rhombo_tetra:
        try:
            struct.type = self.types.index(params['type'])
        except:
            raise ValueError("Addition of unary lattice structure failed.\n"
                             "No corresponding atom type has been defined"
                             " in fit job.")
    elif structure in binary_cubic + binary_hexa_rhombo_tetra:
        try:
            struct.type_A = self.types.index(
                params['type_A'])
            struct.type_B = self.types.index(
                params['type_B'])
        except ValueError:
            raise ValueError("Addition of binary lattice structure failed.\n"
                             "No corresponding atom types have been defined"
                             " in fit job.\n")

    # Set lattice parameters
    if structure in unary_cubic + binary_cubic:
        try:
            struct.lattice_parameter_initial.value = params['alat']
        except:
            raise ValueError("Addition of cubic structure failed."
                             " Lattice parameter invalid.")
    elif structure in unary_hexa_rhombo_tetra + binary_hexa_rhombo_tetra:
        try:
            struct.lattice_parameter_initial.value = params['alat']
        except:
            raise ValueError("Addition of hexagonal/rhombohedral/tetragonal"
                             " structure failed.\n"
                             "Lattice parameter a invalid.")
        try:
            struct.ca_ratio_initial.value = params['ca_ratio']
        except:
            try:
                struct.ca_ratio_initial.value = params[
                    'clat'] / params['alat']
            except:
                raise ValueError("Addition of rhombohedral/hexa-/tetragonal"
                                 " structure failed.\n"
                                 "Lattice parameter c/a invalid.")
    struct.update_structure()

    # Add new structure to structure group.
    if structure_group is None:
        structure_group = self.root_group
    structure_group.registerSubObject(struct, True)

    return struct
Job.add_library_structure = __Job_add_library_structure


# Provide access to structures of FitJob in the form of a dictionary.
def __Job_structures(self):
    """
    dict: structures associated with job.
    """
    local_structures = OrderedDict()
    for structure in self._structures:
        local_structures[structure.id] = structure
    return local_structures
Job.structures = property(__Job_structures)


# Provide access to derivedProperties of FitJob in the form of a dictionary.
def __Job_derived_properties(self):
    """
    dict: derived-properties associated with job.
    """
    derived_properties = OrderedDict()
    for prop in self._derived_properties:
        derived_properties[prop.id] = prop
    return derived_properties
Job.derived_properties = property(__Job_derived_properties)


# Provide access to potentials of FitJob in the form of a dictionary.
def __Job_potentials(self):
    """
    dict: potentials associated with job.
    """
    local_potentials = OrderedDict()
    for pot in self._potentials:
        local_potentials[pot.id] = pot
    return local_potentials
Job.potentials = property(__Job_potentials)


# Calls outputResults of every potential of the job.
def __Job_output_results(self):
    """
    Write resulting potential parameters to file.
    """
    for name, potential in self.potentials.items():
        potential.output_results()
Job.output_results = __Job_output_results


# Calculate the residual.
def __Job_calculate_residual(self, params=None, style='squared'):
    """Calculates the the objective function to be minimized during
    fitting.  This is the weighted sum of the deviations of all
    properties from their target values.

    Parameters
    ----------
    params: list
        parameter vector, if specified it will overwrite the current
        parameter vector associated with this job

    style: string
        method employed for calculating the residual contribution of
        this property to the objective function; possible values are:
        `squared`, `squared_relative`, `user_defined`, `absolute_diff`

    """
    if params is not None:
        self.set_potential_parameters(params)
    if style == 'user_defined':
        return self._calculate_residual(residual_norm.user_defined)
    elif style == 'squared':
        return self._calculate_residual(residual_norm.squared)
    elif style == 'squared_relative':
        return self._calculate_residual(residual_norm.squared_relative)
    elif style == 'absolute_diff':
        return self._calculate_residual(residual_norm.absolute_diff)
    else:
        print('Warning: unknown style for the residual norm', style)
        return
Job.calculate_residual = __Job_calculate_residual


# Nice print potential parameters.
def __Job_print_potential_parameters(self, only_active=False):
    """Print potential parameters associated with job. By default *all*
    parameters (including inactive ones) are written to stdout.

    Parameters
    ----------
    only_active: bool
        If True exclude inactive parameters

    """
    self._print_potential_parameters(not only_active)
Job.print_potential_parameters = __Job_print_potential_parameters


# Nice print properties.
def __Job_print_properties(self):
    """Print properties associated with job.

    """
    self._print_properties()
Job.print_properties = __Job_print_properties
