from ._atomicrex import ScalarFitProperty

def __ScalarFitProperty_repr(self):
    """
    Returns a string representing the fit property.
    """
    lst = []
    lst += [str(self.computed_value)]
    if len(self.units) > 0:
        lst += [self.units]
    if self.fit_enabled:
        lst += ['(target: ', str(self.target_value)]
        if len(self.units) > 0:
            lst += [self.units]
        lst += [')']
        lst += ['Abs. weight: ', str(self.absolute_weight)]
        lst += ['; Tolerance: ', str(self.tolerance)]
        lst += ['; Weighted residual: ']
        lst += [str(self.residual * self.absolute_weight)]
    return ' '.join(lst)
ScalarFitProperty.__repr__ = __ScalarFitProperty_repr
