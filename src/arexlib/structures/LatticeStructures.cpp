///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "LatticeStructures.h"
#include "../job/FitJob.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Constructor.
******************************************************************************/
UnaryCubicLatticeStructure::UnaryCubicLatticeStructure(const FPString& id, FitJob* job)
    : AtomicStructure(id, job),
      _latticeParameter("lattice-parameter", 1, 0),
      _latticeParameterProperty(_latticeParameter, "A", job)
{
    MsgLogger(debug) << "UnaryCubicLatticeStructure: " << id << "   lattice parameter: " << _latticeParameter << endl;
    registerDOF(&_latticeParameter);
    registerProperty(&_latticeParameterProperty);
}

/******************************************************************************
* Parses any structure-specific parameters in the XML element in the job file.
******************************************************************************/
void UnaryCubicLatticeStructure::parse(XML::Element structureElement)
{
    AtomicStructure::parse(structureElement);

    // Parse parameters.
    MsgLogger(debug) << "Parse atom types for unary cubic lattice structure." << endl;
    _atomType = job()->parseAtomTypeElement(structureElement, "atom-type");
    MsgLogger(debug) << "Parse lattice parameter for unary cubic lattice structure." << endl;
    _latticeParameter.setInitialValue(structureElement.parseFloatParameterElement("lattice-parameter"));
}

/******************************************************************************
* Constructor.
******************************************************************************/
BinaryCubicLatticeStructure::BinaryCubicLatticeStructure(const FPString& id, FitJob* job)
    : AtomicStructure(id, job),
      _latticeParameter("lattice-parameter", 1, 0),
      _latticeParameterProperty(_latticeParameter, "A", job)
{
    registerDOF(&_latticeParameter);
    registerProperty(&_latticeParameterProperty);
}

/******************************************************************************
* Parses any structure-specific parameters in the XML element in the job file.
******************************************************************************/
void BinaryCubicLatticeStructure::parse(XML::Element structureElement)
{
    AtomicStructure::parse(structureElement);

    // Parse parameters.
    MsgLogger(debug) << "Parse atom types for binary cubic lattice structure." << endl;
    _atomTypeA = job()->parseAtomTypeElement(structureElement, "atom-type-A");
    _atomTypeB = job()->parseAtomTypeElement(structureElement, "atom-type-B");
    MsgLogger(debug) << "Parse lattice parameter for binary cubic lattice structure." << endl;
    _latticeParameter.setInitialValue(structureElement.parseFloatParameterElement("lattice-parameter"));
}

/******************************************************************************
* Constructor.
******************************************************************************/
UnaryHexaRhomboTetraLatticeStructure::UnaryHexaRhomboTetraLatticeStructure(const FPString& id, FitJob* job)
    : AtomicStructure(id, job),
      _latticeParameter("lattice-parameter"),
      _latticeParameterProperty(_latticeParameter, "A", job),
      _CtoAratio("ca-ratio"),
      _CtoAratioProperty(_CtoAratio, FPString(), job)
{
    registerDOF(&_latticeParameter);
    registerDOF(&_CtoAratio);
    registerProperty(&_latticeParameterProperty);
    registerProperty(&_CtoAratioProperty);
}

/******************************************************************************
* Parses any structure-specific parameters in the XML element in the job file.
******************************************************************************/
void UnaryHexaRhomboTetraLatticeStructure::parse(XML::Element structureElement)
{
    AtomicStructure::parse(structureElement);

    // Parse parameters.
    MsgLogger(debug) << "Parse atom types for unary hexagonal/rhombohedral/tetragonal structure." << endl;
    _atomType = job()->parseAtomTypeElement(structureElement, "atom-type");
    MsgLogger(debug) << "Parse lattice parameters for unary hexagonal/rhombohedral/tetragonal structure." << endl;
    _latticeParameter.setInitialValue(structureElement.parseFloatParameterElement("lattice-parameter"));
    _CtoAratio.setInitialValue(structureElement.parseFloatParameterElement("ca-ratio"));
}

/******************************************************************************
* Constructor.
******************************************************************************/
BinaryHexaRhomboTetraLatticeStructure::BinaryHexaRhomboTetraLatticeStructure(const FPString& id, FitJob* job)
    : AtomicStructure(id, job),
      _latticeParameter("lattice-parameter", 1, 0),
      _latticeParameterProperty(_latticeParameter, "A", job),
      _CtoAratio("ca-ratio"),
      _CtoAratioProperty(_CtoAratio, FPString(), job)
{
    registerDOF(&_latticeParameter);
    registerDOF(&_CtoAratio);
    registerProperty(&_latticeParameterProperty);
    registerProperty(&_CtoAratioProperty);
}

/******************************************************************************
* Parses any structure-specific parameters in the XML element in the job file.
******************************************************************************/
void BinaryHexaRhomboTetraLatticeStructure::parse(XML::Element structureElement)
{
    AtomicStructure::parse(structureElement);

    // Parse parameters.
    MsgLogger(debug) << "Parse atom types for binary hexagonal/rhombohedral/tetragonal structure." << endl;
    _atomTypeA = job()->parseAtomTypeElement(structureElement, "atom-type-A");
    _atomTypeB = job()->parseAtomTypeElement(structureElement, "atom-type-B");
    MsgLogger(debug) << "Parse lattice parameters for binary hexagonal/rhombohedral/tetragonal structure." << endl;
    _latticeParameter.setInitialValue(structureElement.parseFloatParameterElement("lattice-parameter"));
    _CtoAratio.setInitialValue(structureElement.parseFloatParameterElement("ca-ratio"));
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void FCCLatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        MsgLogger(debug) << "Updating FCC structure. Lattice parameter: " << setprecision(_MsgLoggerPrecision) << latticeParameter() << endl;

        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(0, 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0,
                                    0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0),
                            Point3::Origin(), {true, true, true});

        // Create a single atom.
        setAtomCount(1);
        atomPositions()[0] = Point3::Origin();
        atomTypes()[0] = atomType();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    UnaryCubicLatticeStructure::updateStructure();
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void BCCLatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(-0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.5 * latticeParameter(),
                                    0.5 * latticeParameter(), -0.5 * latticeParameter(), 0.5 * latticeParameter(),
                                    0.5 * latticeParameter(), 0.5 * latticeParameter(), -0.5 * latticeParameter()),
                            Point3::Origin(), {true, true, true});

        // Create a single atom.
        setAtomCount(1);
        atomPositions()[0] = Point3::Origin();
        atomTypes()[0] = atomType();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    UnaryCubicLatticeStructure::updateStructure();
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void HCPLatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        double angle = 60.0 * FLOATTYPE_PI / 180.0;
        double c = cos(angle);
        double s = sin(angle);
        Matrix3 rotation(c, -s, 0.0, s, c, 0.0, 0.0, 0.0, 1.0);

        // Setup primitive lattice cell.
        setupSimulationCell(
            rotation * Matrix3(0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.0,
                               -0.86602540378443864676 * latticeParameter(), +0.86602540378443864676 * latticeParameter(), 0.0,
                               0.0, 0.0, latticeParameter() * CtoAratio()),
            Point3::Origin(), {true, true, true});

        // Create atoms.
        setAtomCount(2);
        atomPositions()[0] = simulationCell() * Point3(1.0 / 3.0, 2.0 / 3.0, 0.25);
        atomPositions()[1] = simulationCell() * Point3(2.0 / 3.0, 1.0 / 3.0, 0.75);
        atomTypes()[0] = atomType();
        atomTypes()[1] = atomType();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    UnaryHexaRhomboTetraLatticeStructure::updateStructure();
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void DHCPLatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        double angle = 60.0 * FLOATTYPE_PI / 180.0;
        double c = cos(angle);
        double s = sin(angle);
        Matrix3 rotation(c, -s, 0.0, s, c, 0.0, 0.0, 0.0, 1.0);

        // Setup primitive lattice cell.
        setupSimulationCell(rotation * Matrix3(0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.0,
                                               -0.86602540378443864676 * latticeParameter(),
                                               +0.86602540378443864676 * latticeParameter(), 0.0, 0.0, 0.0,
                                               latticeParameter() * CtoAratio()));

        // Create atoms.
        setAtomCount(4);
        atomPositions()[0] = simulationCell() * Point3(1.0 / 3.0, 2.0 / 3.0, 0.125);
        atomPositions()[1] = simulationCell() * Point3(2.0 / 3.0, 1.0 / 3.0, 0.375);
        atomPositions()[2] = simulationCell() * Point3(1.0 / 3.0, 2.0 / 3.0, 0.625);
        atomPositions()[3] = simulationCell() * Point3(0.0 / 3.0, 0.0 / 3.0, 0.875);
        atomTypes()[0] = atomType();
        atomTypes()[1] = atomType();
        atomTypes()[2] = atomType();
        atomTypes()[3] = atomType();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    UnaryHexaRhomboTetraLatticeStructure::updateStructure();
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void betaSnLatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup big lattice cell.
        setupSimulationCell(
            Matrix3(latticeParameter(), 0.0, 0.0, 0.0, latticeParameter(), 0.0, 0.0, 0.0, latticeParameter() * CtoAratio()));

        // Create atoms.
        setAtomCount(4);
        atomPositions()[0] = simulationCell() * Point3(0.0, 0.0, 0.0);
        atomPositions()[1] = simulationCell() * Point3(0.5, 0.5, 0.5);
        atomPositions()[2] = simulationCell() * Point3(0.0, 0.5, 0.25);
        atomPositions()[3] = simulationCell() * Point3(0.5, 0.0, 0.75);
        atomTypes()[0] = atomType();
        atomTypes()[1] = atomType();
        atomTypes()[2] = atomType();
        atomTypes()[3] = atomType();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    UnaryHexaRhomboTetraLatticeStructure::updateStructure();
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void DIALatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(0, 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0,
                                    0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0));

        // Create atoms.
        setAtomCount(2);
        atomPositions()[0] = simulationCell() * Point3(0, 0, 0);
        atomPositions()[1] = simulationCell() * Point3(0.25, 0.25, 0.25);
        atomTypes()[0] = atomType();
        atomTypes()[1] = atomType();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    UnaryCubicLatticeStructure::updateStructure();
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void SCLatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(latticeParameter(), 0, 0, 0, latticeParameter(), 0, 0, 0, latticeParameter()));

        // Create a single atom.
        setAtomCount(1);
        atomPositions()[0] = simulationCell() * Point3(0, 0, 0);
        atomTypes()[0] = atomType();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    UnaryCubicLatticeStructure::updateStructure();
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void OMGLatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        double angle = 60.0 * FLOATTYPE_PI / 180.0;
        double c = cos(angle);
        double s = sin(angle);
        Matrix3 rotation(c, -s, 0.0, s, c, 0.0, 0.0, 0.0, 1.0);

        // Setup primitive lattice cell.
        setupSimulationCell(rotation * Matrix3(0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.0,
                                               -0.86602540378443864676 * latticeParameter(),
                                               +0.86602540378443864676 * latticeParameter(), 0.0, 0.0, 0.0,
                                               latticeParameter() * CtoAratio()));

        // Create a single atom.
        setAtomCount(3);
        atomPositions()[0] = simulationCell() * Point3(0, 0, 0);
        atomPositions()[1] = simulationCell() * Point3(2.0 / 3.0, 1.0 / 3.0, 0.5);
        atomPositions()[2] = simulationCell() * Point3(1.0 / 3.0, 2.0 / 3.0, 0.5);
        atomTypes()[0] = atomType();
        atomTypes()[1] = atomType();
        atomTypes()[2] = atomType();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    UnaryHexaRhomboTetraLatticeStructure::updateStructure();
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void B1LatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(0, 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0,
                                    0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0));

        // Create a single atom.
        setAtomCount(2);
        atomPositions()[0] = simulationCell() * Point3(0, 0, 0);
        atomPositions()[1] = simulationCell() * Point3(0.5, 0.5, 0.5);
        atomTypes()[0] = atomTypeA();
        atomTypes()[1] = atomTypeB();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    BinaryCubicLatticeStructure::updateStructure();
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void B2LatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(latticeParameter(), 0, 0, 0, latticeParameter(), 0, 0, 0, latticeParameter()));

        // Create a single atom.
        setAtomCount(2);
        atomPositions()[0] = simulationCell() * Point3(0, 0, 0);
        atomPositions()[1] = simulationCell() * Point3(0.5, 0.5, 0.5);
        atomTypes()[0] = atomTypeA();
        atomTypes()[1] = atomTypeB();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    BinaryCubicLatticeStructure::updateStructure();
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void B3LatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(0, 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0,
                                    0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0));

        // Create a single atom.
        setAtomCount(2);
        atomPositions()[0] = simulationCell() * Point3(0, 0, 0);
        atomPositions()[1] = simulationCell() * Point3(0.25, 0.25, 0.25);
        atomTypes()[0] = atomTypeA();
        atomTypes()[1] = atomTypeB();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    BinaryCubicLatticeStructure::updateStructure();
}

/******************************************************************************
* Constructor.
******************************************************************************/
B4LatticeStructure::B4LatticeStructure(const FPString& id, FitJob* job)
    : BinaryHexaRhomboTetraLatticeStructure(id, job),
      _internalParameterU("u-parameter"),
      _internalParameterUProperty(_internalParameterU, FPString(), job)
{
    registerDOF(&_internalParameterU);
    registerProperty(&_internalParameterUProperty);
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void B4LatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        double angle = 60.0 * FLOATTYPE_PI / 180.0;
        double c = cos(angle);
        double s = sin(angle);
        Matrix3 rotation(c, -s, 0.0, s, c, 0.0, 0.0, 0.0, 1.0);

        // Setup primitive lattice cell.
        setupSimulationCell(rotation * Matrix3(0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.0,
                                               -0.86602540378443864676 * latticeParameter(),
                                               +0.86602540378443864676 * latticeParameter(), 0.0, 0.0, 0.0,
                                               latticeParameter() * CtoAratio()));

        // Create a single atom.
        setAtomCount(4);
        atomPositions()[0] = simulationCell() * Point3(1.0 / 3.0, 2.0 / 3.0, 0.0);
        atomPositions()[1] = simulationCell() * Point3(2.0 / 3.0, 1.0 / 3.0, 0.5);
        atomPositions()[2] = simulationCell() * Point3(1.0 / 3.0, 2.0 / 3.0, 0.0 + internalParameterU());
        atomPositions()[3] = simulationCell() * Point3(2.0 / 3.0, 1.0 / 3.0, 0.5 + internalParameterU());
        atomTypes()[0] = atomTypeA();
        atomTypes()[1] = atomTypeA();
        atomTypes()[2] = atomTypeB();
        atomTypes()[3] = atomTypeB();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    BinaryHexaRhomboTetraLatticeStructure::updateStructure();
}

/******************************************************************************
* Parses any structure-specific parameters in the XML element in the job file.
******************************************************************************/
void B4LatticeStructure::parse(XML::Element structureElement)
{
    BinaryHexaRhomboTetraLatticeStructure::parse(structureElement);

    // Parse parameters.
    MsgLogger(debug) << "Parse B4 internal parameter." << endl;
    _internalParameterU.setInitialValue(structureElement.parseFloatParameterElement("u-parameter"));
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void C1LatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(0, 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0,
                                    0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0));

        // Create a single atom.
        setAtomCount(3);
        atomPositions()[0] = simulationCell() * Point3(0, 0, 0);
        atomPositions()[1] = simulationCell() * Point3(-0.25, -0.25, -0.25);
        atomPositions()[2] = simulationCell() * Point3(0.25, 0.25, 0.25);
        atomTypes()[0] = atomTypeA();
        atomTypes()[1] = atomTypeB();
        atomTypes()[2] = atomTypeB();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    BinaryCubicLatticeStructure::updateStructure();
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void C15LatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(0, 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0,
                                    0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0));

        // Create a single atom.
        setAtomCount(6);
        atomPositions()[0] = simulationCell() * Point3(0, 0, 0);
        atomPositions()[1] = simulationCell() * Point3(0.25, 0.25, 0.25);
        atomPositions()[2] = simulationCell() * Point3(0.625, 0.625, 0.625);
        atomPositions()[3] = simulationCell() * Point3(0.625, 0.625, 0.125);
        atomPositions()[4] = simulationCell() * Point3(0.625, 0.125, 0.625);
        atomPositions()[5] = simulationCell() * Point3(0.125, 0.625, 0.625);
        atomTypes()[0] = atomTypeB();
        atomTypes()[1] = atomTypeB();
        atomTypes()[2] = atomTypeA();
        atomTypes()[3] = atomTypeA();
        atomTypes()[4] = atomTypeA();
        atomTypes()[5] = atomTypeA();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    BinaryCubicLatticeStructure::updateStructure();
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void D2dLatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(latticeParameter(), -0.5 * latticeParameter(), 0, 0, sqrt(3) / 2 * latticeParameter(), 0, 0,
                                    0, latticeParameter() * CtoAratio()));

        // Create a single atom.
        setAtomCount(6);
        atomPositions()[0] = simulationCell() * Point3(0.0, 0.0, 0.0);
        atomPositions()[1] = simulationCell() * Point3(1.0 / 3.0, 2.0 / 3.0, 0);
        atomPositions()[2] = simulationCell() * Point3(2.0 / 3.0, 1.0 / 3.0, 0);
        atomPositions()[3] = simulationCell() * Point3(0.5, 0.0, 0.5);
        atomPositions()[4] = simulationCell() * Point3(0.0, 0.5, 0.5);
        atomPositions()[5] = simulationCell() * Point3(0.5, 0.5, 0.5);
        atomTypes()[0] = atomTypeB();
        atomTypes()[1] = atomTypeA();
        atomTypes()[2] = atomTypeA();
        atomTypes()[3] = atomTypeA();
        atomTypes()[4] = atomTypeA();
        atomTypes()[5] = atomTypeA();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    BinaryHexaRhomboTetraLatticeStructure::updateStructure();
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void Ni17Th2LatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(latticeParameter(), -0.5 * latticeParameter(), 0, 0, sqrt(3) / 2 * latticeParameter(), 0, 0,
                                    0, latticeParameter() * CtoAratio()));

        double a = 0.11;
        double b = 0.39;

        // Create the atoms.
        setAtomCount(38);
        atomPositions()[0] = simulationCell() * Point3(0.0, 0.0, 1.0 / 4.0);
        atomPositions()[1] = simulationCell() * Point3(0.0, 0.0, 3.0 / 4.0);
        atomPositions()[2] = simulationCell() * Point3(1.0 / 3.0, 2.0 / 3.0, 3.0 / 4.0);
        atomPositions()[3] = simulationCell() * Point3(2.0 / 3.0, 1.0 / 3.0, 1.0 / 4.0);
        atomPositions()[4] = simulationCell() * Point3(0.5, 0.0, 0.0);
        atomPositions()[5] = simulationCell() * Point3(0.0, 0.5, 0.0);
        atomPositions()[6] = simulationCell() * Point3(0.5, 0.5, 0.0);
        atomPositions()[7] = simulationCell() * Point3(0.5, 0.0, 0.5);
        atomPositions()[8] = simulationCell() * Point3(0.0, 0.5, 0.5);
        atomPositions()[9] = simulationCell() * Point3(0.5, 0.5, 0.5);
        atomPositions()[10] = simulationCell() * Point3(1.0 / 3.0, 2.0 / 3.0, a);
        atomPositions()[11] = simulationCell() * Point3(2.0 / 3.0, 1.0 / 3.0, a + 0.5);
        atomPositions()[12] = simulationCell() * Point3(2.0 / 3.0, 1.0 / 3.0, b + 0.5);
        atomPositions()[13] = simulationCell() * Point3(1.0 / 3.0, 2.0 / 3.0, b);
        atomPositions()[14] = simulationCell() * Point3(1.0 / 3.0, 0.0, 1.0 / 4.0);
        atomPositions()[15] = simulationCell() * Point3(0.0, 1.0 / 3.0, 1.0 / 4.0);
        atomPositions()[16] = simulationCell() * Point3(2.0 / 3.0, 2.0 / 3.0, 1.0 / 4.0);
        atomPositions()[17] = simulationCell() * Point3(2.0 / 3.0, 0.0, 3.0 / 4.0);
        atomPositions()[18] = simulationCell() * Point3(0.0, 2.0 / 3.0, 3.0 / 4.0);
        atomPositions()[19] = simulationCell() * Point3(1.0 / 3.0, 1.0 / 3.0, 3.0 / 4.0);
        atomPositions()[20] = simulationCell() * Point3(0.0, 1.0 / 3.0, 3.0 / 4.0);
        atomPositions()[21] = simulationCell() * Point3(1.0 / 3.0, 0.0, 3.0 / 4.0);
        atomPositions()[22] = simulationCell() * Point3(2.0 / 3.0, 2.0 / 3.0, 3.0 / 4.0);
        atomPositions()[23] = simulationCell() * Point3(0.0, 2.0 / 3.0, 1.0 / 4.0);
        atomPositions()[24] = simulationCell() * Point3(2.0 / 3.0, 0.0, 1.0 / 4.0);
        atomPositions()[25] = simulationCell() * Point3(1.0 / 3.0, 1.0 / 3.0, 1.0 / 4.0);
        atomPositions()[26] = simulationCell() * Point3(1.0 / 6.0, 1.0 / 3.0, 0.0);
        atomPositions()[27] = simulationCell() * Point3(2.0 / 3.0, 5.0 / 6.0, 0.0);
        atomPositions()[28] = simulationCell() * Point3(1.0 / 6.0, 5.0 / 6.0, 0.0);
        atomPositions()[29] = simulationCell() * Point3(5.0 / 6.0, 2.0 / 3.0, 0.5);
        atomPositions()[30] = simulationCell() * Point3(1.0 / 3.0, 1.0 / 6.0, 0.5);
        atomPositions()[31] = simulationCell() * Point3(5.0 / 6.0, 1.0 / 6.0, 0.5);
        atomPositions()[32] = simulationCell() * Point3(1.0 / 3.0, 1.0 / 6.0, 0.0);
        atomPositions()[33] = simulationCell() * Point3(5.0 / 6.0, 2.0 / 3.0, 0.0);
        atomPositions()[34] = simulationCell() * Point3(5.0 / 6.0, 1.0 / 6.0, 0.0);
        atomPositions()[35] = simulationCell() * Point3(2.0 / 3.0, 5.0 / 6.0, 0.5);
        atomPositions()[36] = simulationCell() * Point3(1.0 / 6.0, 1.0 / 3.0, 0.5);
        atomPositions()[37] = simulationCell() * Point3(1.0 / 6.0, 5.0 / 6.0, 0.5);

        atomTypes()[0] = atomTypeB();
        atomTypes()[1] = atomTypeB();
        atomTypes()[2] = atomTypeB();
        atomTypes()[3] = atomTypeB();
        atomTypes()[4] = atomTypeA();
        atomTypes()[5] = atomTypeA();
        atomTypes()[6] = atomTypeA();
        atomTypes()[7] = atomTypeA();
        atomTypes()[8] = atomTypeA();
        atomTypes()[9] = atomTypeA();
        atomTypes()[10] = atomTypeA();
        atomTypes()[11] = atomTypeA();
        atomTypes()[12] = atomTypeA();
        atomTypes()[13] = atomTypeA();
        atomTypes()[14] = atomTypeA();
        atomTypes()[15] = atomTypeA();
        atomTypes()[16] = atomTypeA();
        atomTypes()[17] = atomTypeA();
        atomTypes()[18] = atomTypeA();
        atomTypes()[19] = atomTypeA();
        atomTypes()[20] = atomTypeA();
        atomTypes()[21] = atomTypeA();
        atomTypes()[22] = atomTypeA();
        atomTypes()[23] = atomTypeA();
        atomTypes()[24] = atomTypeA();
        atomTypes()[25] = atomTypeA();
        atomTypes()[26] = atomTypeA();
        atomTypes()[27] = atomTypeA();
        atomTypes()[28] = atomTypeA();
        atomTypes()[29] = atomTypeA();
        atomTypes()[30] = atomTypeA();
        atomTypes()[31] = atomTypeA();
        atomTypes()[32] = atomTypeA();
        atomTypes()[33] = atomTypeA();
        atomTypes()[34] = atomTypeA();
        atomTypes()[35] = atomTypeA();
        atomTypes()[36] = atomTypeA();
        atomTypes()[37] = atomTypeA();

        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    BinaryHexaRhomboTetraLatticeStructure::updateStructure();
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void Th2Zn17LatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(latticeParameter(), -0.5 * latticeParameter(), 0, 0, sqrt(3) / 2 * latticeParameter(), 0, 0,
                                    0, latticeParameter() * CtoAratio()));

        double a = 0.097;
        double b = 0.903;
        double c = 0.4303333333;
        double d = 0.2363333333;
        double e = 0.7636666666999999;
        double f = 0.5696666667;
        // Create the atoms.
        setAtomCount(57);
        atomPositions()[0] = simulationCell() * Point3(1.0 / 3.0, 0.0, 2.0 / 3.0);
        atomPositions()[1] = simulationCell() * Point3(2.0 / 3.0, 0.0, 0.0);
        atomPositions()[2] = simulationCell() * Point3(0.0, 2.0 / 3.0, 0.0);
        atomPositions()[3] = simulationCell() * Point3(1.0 / 3.0, 1.0 / 3.0, 0.0);
        atomPositions()[4] = simulationCell() * Point3(1.0 / 3.0, 1.0 / 3.0, 1.0 / 3.0);
        atomPositions()[5] = simulationCell() * Point3(2.0 / 3.0, 0.0, 1.0 / 3.0);
        atomPositions()[6] = simulationCell() * Point3(0.0, 2.0 / 3.0, 1.0 / 3.0);
        atomPositions()[7] = simulationCell() * Point3(0.0, 2.0 / 3.0, 2.0 / 3.0);
        atomPositions()[8] = simulationCell() * Point3(1.0 / 3.0, 1.0 / 3.0, 2.0 / 3.0);
        atomPositions()[9] = simulationCell() * Point3(2.0 / 3.0, 0.0, 2.0 / 3.0);
        atomPositions()[10] = simulationCell() * Point3(0.5, 0.5, 1.0 / 6.0);
        atomPositions()[11] = simulationCell() * Point3(0.5, 0.0, 1.0 / 6.0);
        atomPositions()[12] = simulationCell() * Point3(0.0, 0.5, 1.0 / 6.0);
        atomPositions()[13] = simulationCell() * Point3(0.5, 0.5, 5.0 / 6.0);
        atomPositions()[14] = simulationCell() * Point3(0.0, 0.5, 5.0 / 6.0);
        atomPositions()[15] = simulationCell() * Point3(0.5, 0.0, 5.0 / 6.0);
        atomPositions()[16] = simulationCell() * Point3(1.0 / 6.0, 5.0 / 6.0, 0.5);
        atomPositions()[17] = simulationCell() * Point3(1.0 / 6.0, 1.0 / 3.0, 0.5);
        atomPositions()[18] = simulationCell() * Point3(2.0 / 3.0, 5.0 / 6.0, 0.5);
        atomPositions()[19] = simulationCell() * Point3(1.0 / 6.0, 5.0 / 6.0, 1.0 / 6.0);
        atomPositions()[20] = simulationCell() * Point3(2.0 / 3.0, 5.0 / 6.0, 1.0 / 6.0);
        atomPositions()[21] = simulationCell() * Point3(1.0 / 6.0, 1.0 / 3.0, 1.0 / 6.0);
        atomPositions()[22] = simulationCell() * Point3(5.0 / 6.0, 1.0 / 6.0, 5.0 / 6.0);
        atomPositions()[23] = simulationCell() * Point3(5.0 / 6.0, 2.0 / 3.0, 5.0 / 6.0);
        atomPositions()[24] = simulationCell() * Point3(1.0 / 3.0, 1.0 / 6.0, 5.0 / 6.0);
        atomPositions()[25] = simulationCell() * Point3(5.0 / 6.0, 1.0 / 6.0, 0.5);
        atomPositions()[26] = simulationCell() * Point3(0.0, 1.0 / 3.0, 2.0 / 3.0);
        atomPositions()[27] = simulationCell() * Point3(1.0 / 3.0, 1.0 / 6.0, 0.5);
        atomPositions()[28] = simulationCell() * Point3(5.0 / 6.0, 2.0 / 3.0, 0.5);
        atomPositions()[29] = simulationCell() * Point3(1.0 / 3.0, 0.0, 1.0 / 3.0);
        atomPositions()[30] = simulationCell() * Point3(0.0, 0.0, a);
        atomPositions()[31] = simulationCell() * Point3(0.0, 0.0, b);
        atomPositions()[32] = simulationCell() * Point3(2.0 / 3.0, 1.0 / 3.0, c);
        atomPositions()[33] = simulationCell() * Point3(2.0 / 3.0, 1.0 / 3.0, d);
        atomPositions()[34] = simulationCell() * Point3(1.0 / 3.0, 2.0 / 3.0, e);
        atomPositions()[35] = simulationCell() * Point3(1.0 / 3.0, 2.0 / 3.0, f);
        atomPositions()[36] = simulationCell() * Point3(0.5, 0.0, 0.5);
        atomPositions()[37] = simulationCell() * Point3(2.0 / 3.0, 2.0 / 3.0, 2.0 / 3.0);
        atomPositions()[38] = simulationCell() * Point3(0.5, 0.5, 0.5);
        atomPositions()[39] = simulationCell() * Point3(1.0 / 6.0, 1.0 / 3.0, 5.0 / 6.0);
        atomPositions()[40] = simulationCell() * Point3(0.0, 0.5, 0.5);
        atomPositions()[41] = simulationCell() * Point3(1.0 / 6.0, 5.0 / 6.0, 5.0 / 6.0);
        atomPositions()[42] = simulationCell() * Point3(5.0 / 6.0, 2.0 / 3.0, 1.0 / 6.0);
        atomPositions()[43] = simulationCell() * Point3(1.0 / 3.0, 1.0 / 6.0, 1.0 / 6.0);
        atomPositions()[44] = simulationCell() * Point3(5.0 / 6.0, 1.0 / 6.0, 1.0 / 6.0);
        atomPositions()[45] = simulationCell() * Point3(1.0 / 3.0, 0.0, 0.0);
        atomPositions()[46] = simulationCell() * Point3(0.0, 1.0 / 3.0, 0.0);
        atomPositions()[47] = simulationCell() * Point3(2.0 / 3.0, 2.0 / 3.0, 0.0);
        atomPositions()[48] = simulationCell() * Point3(0.0, 1.0 / 3.0, 1.0 / 3.0);
        atomPositions()[49] = simulationCell() * Point3(2.0 / 3.0, 2.0 / 3.0, 1.0 / 3.0);
        atomPositions()[50] = simulationCell() * Point3(2.0 / 3.0, 5.0 / 6.0, 5.0 / 6.0);
        atomPositions()[51] = simulationCell() * Point3(1.0 / 3.0, 2.0 / 3.0, 1.0 / 3.0);
        atomPositions()[52] = simulationCell() * Point3(1.0 / 3.0, 2.0 / 3.0, 0.0);
        atomPositions()[53] = simulationCell() * Point3(2.0 / 3.0, 1.0 / 3.0, 0.0);
        atomPositions()[54] = simulationCell() * Point3(2.0 / 3.0, 1.0 / 3.0, 2.0 / 3.0);
        atomPositions()[55] = simulationCell() * Point3(0.0, 0.0, 2.0 / 3.0);
        atomPositions()[56] = simulationCell() * Point3(0.0, 0.0, 1.0 / 3.0);

        for(int i = 0; i < 57; i++) {
            if(i < 51)
                atomTypes()[i] = atomTypeA();
            else
                atomTypes()[i] = atomTypeB();
        }

        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    BinaryHexaRhomboTetraLatticeStructure::updateStructure();
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void D8aLatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(0, 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0,
                                    0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.5 * latticeParameter(), 0));

        double a = 0.797;
        double b = 0.203;
        double c = 0.178;
        double d = 0.822;
        double e = 0.466;
        double g = 0.534;
        double h = 0.378;
        double i = 0.866;
        double j = 0.622;
        double k = 0.134;

        // Create a single atom.
        setAtomCount(29);
        atomPositions()[0] = simulationCell() * Point3(a, b, b);
        atomPositions()[1] = simulationCell() * Point3(b, a, a);
        atomPositions()[2] = simulationCell() * Point3(b, a, b);
        atomPositions()[3] = simulationCell() * Point3(a, b, a);
        atomPositions()[4] = simulationCell() * Point3(b, b, a);
        atomPositions()[5] = simulationCell() * Point3(a, a, b);
        atomPositions()[6] = simulationCell() * Point3(0.5, 0.5, 0.5);
        atomPositions()[7] = simulationCell() * Point3(0.5, 0.0, 0.0);
        atomPositions()[8] = simulationCell() * Point3(0.0, 0.5, 0.0);
        atomPositions()[9] = simulationCell() * Point3(0.0, 0.0, 0.5);
        atomPositions()[10] = simulationCell() * Point3(0.5, 0.0, 0.5);
        atomPositions()[11] = simulationCell() * Point3(0.5, 0.5, 0.0);
        atomPositions()[12] = simulationCell() * Point3(0.0, 0.5, 0.5);
        atomPositions()[13] = simulationCell() * Point3(c, c, c);
        atomPositions()[14] = simulationCell() * Point3(d, d, d);
        atomPositions()[15] = simulationCell() * Point3(e, c, c);
        atomPositions()[16] = simulationCell() * Point3(g, d, d);
        atomPositions()[17] = simulationCell() * Point3(c, e, c);
        atomPositions()[18] = simulationCell() * Point3(d, g, d);
        atomPositions()[19] = simulationCell() * Point3(c, c, e);
        atomPositions()[20] = simulationCell() * Point3(d, d, g);
        atomPositions()[21] = simulationCell() * Point3(h, h, h);
        atomPositions()[22] = simulationCell() * Point3(h, h, i);
        atomPositions()[23] = simulationCell() * Point3(h, i, h);
        atomPositions()[24] = simulationCell() * Point3(i, h, h);
        atomPositions()[25] = simulationCell() * Point3(j, j, k);
        atomPositions()[26] = simulationCell() * Point3(j, j, j);
        atomPositions()[27] = simulationCell() * Point3(j, k, j);
        atomPositions()[28] = simulationCell() * Point3(k, j, j);
        atomTypes()[0] = atomTypeB();
        atomTypes()[1] = atomTypeB();
        atomTypes()[2] = atomTypeB();
        atomTypes()[3] = atomTypeB();
        atomTypes()[4] = atomTypeB();
        atomTypes()[5] = atomTypeB();
        atomTypes()[6] = atomTypeA();
        atomTypes()[7] = atomTypeA();
        atomTypes()[8] = atomTypeA();
        atomTypes()[9] = atomTypeA();
        atomTypes()[10] = atomTypeA();
        atomTypes()[11] = atomTypeA();
        atomTypes()[12] = atomTypeA();
        atomTypes()[13] = atomTypeA();
        atomTypes()[14] = atomTypeA();
        atomTypes()[15] = atomTypeA();
        atomTypes()[16] = atomTypeA();
        atomTypes()[17] = atomTypeA();
        atomTypes()[18] = atomTypeA();
        atomTypes()[19] = atomTypeA();
        atomTypes()[20] = atomTypeA();
        atomTypes()[21] = atomTypeA();
        atomTypes()[22] = atomTypeA();
        atomTypes()[23] = atomTypeA();
        atomTypes()[24] = atomTypeA();
        atomTypes()[25] = atomTypeA();
        atomTypes()[26] = atomTypeA();
        atomTypes()[27] = atomTypeA();
        atomTypes()[28] = atomTypeA();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    BinaryCubicLatticeStructure::updateStructure();
}

/******************************************************************************
 * Updates the structure (atom positions, simulation cell, etc.)
 ******************************************************************************/
void L12LatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(latticeParameter(), 0, 0, 0, latticeParameter(), 0, 0, 0, latticeParameter()));

        // Create a single atom.
        setAtomCount(4);
        atomPositions()[0] = simulationCell() * Point3(0, 0, 0);
        atomPositions()[1] = simulationCell() * Point3(0, 0.5, 0.5);
        atomPositions()[2] = simulationCell() * Point3(0.5, 0, 0.5);
        atomPositions()[3] = simulationCell() * Point3(0.5, 0.5, 0);
        atomTypes()[0] = atomTypeA();
        atomTypes()[1] = atomTypeB();
        atomTypes()[2] = atomTypeB();
        atomTypes()[3] = atomTypeB();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    BinaryCubicLatticeStructure::updateStructure();
}

/******************************************************************************
 * Updates the structure (atom positions, simulation cell, etc.)
 ******************************************************************************/
void L10LatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(latticeParameter(), 0, 0, 0, latticeParameter(), 0, 0, 0, latticeParameter() * CtoAratio()));

        // Create a single atom.
        setAtomCount(4);
        atomPositions()[0] = simulationCell() * Point3(0, 0, 0);
        atomPositions()[1] = simulationCell() * Point3(0, 0.5, 0.5);
        atomPositions()[2] = simulationCell() * Point3(0.5, 0, 0.5);
        atomPositions()[3] = simulationCell() * Point3(0.5, 0.5, 0);
        atomTypes()[0] = atomTypeA();
        atomTypes()[1] = atomTypeA();
        atomTypes()[2] = atomTypeB();
        atomTypes()[3] = atomTypeB();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    BinaryHexaRhomboTetraLatticeStructure::updateStructure();
}

/******************************************************************************
 * Updates the structure (atom positions, simulation cell, etc.)
 ******************************************************************************/
void BhLatticeStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        double angle = 60.0 * FLOATTYPE_PI / 180.0;
        double c = cos(angle);
        double s = sin(angle);
        Matrix3 rotation(c, -s, 0.0, s, c, 0.0, 0.0, 0.0, 1.0);

        // Setup primitive lattice cell.
        setupSimulationCell(rotation * Matrix3(0.5 * latticeParameter(), 0.5 * latticeParameter(), 0.0,
                                               -0.86602540378443864676 * latticeParameter(),
                                               +0.86602540378443864676 * latticeParameter(), 0.0, 0.0, 0.0,
                                               latticeParameter() * CtoAratio()));

        // Create a single atom.
        setAtomCount(2);
        atomPositions()[0] = simulationCell() * Point3(1.0 / 3.0, 2.0 / 3.0, 0.25);
        atomPositions()[1] = simulationCell() * Point3(2.0 / 3.0, 1.0 / 3.0, 0.75);
        atomTypes()[0] = atomTypeA();
        atomTypes()[1] = atomTypeB();
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    BinaryHexaRhomboTetraLatticeStructure::updateStructure();
}
}
