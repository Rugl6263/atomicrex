///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "FitObject.h"
#include "../dof/DegreeOfFreedom.h"
#include "../properties/FitProperty.h"
#include "../job/FitJob.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Registers a DOF of the object.
******************************************************************************/
void FitObject::registerDOF(DegreeOfFreedom* dof)
{
    MsgLogger(debug) << "registerDOF: " << dof->id() << endl;
    _dofList.push_back(dof);
    BOOST_ASSERT(dof->_object == nullptr);
    dof->_object = this;
}

/******************************************************************************
* Registers a fitting property of the object.
******************************************************************************/
void FitObject::registerProperty(FitProperty* prop, bool deleteOnShutdown)
{
    MsgLogger(debug) << "registerProperty: " << prop->id() << endl;
    _fitPropertiesList.push_back(prop);
    registerSubObject(prop, deleteOnShutdown);
}

/******************************************************************************
* Registers a sub-object.
******************************************************************************/
void FitObject::registerSubObject(FitObject* subobject, bool deleteOnShutdown)
{
    BOOST_ASSERT(subobject->job() == this->job());
    BOOST_ASSERT(subobject->_parent == nullptr);
    _subobjectList.push_back(subobject);
    subobject->_parent = this;
    MsgLogger(debug) << "Registered subobject " << subobject->id() << " on" << id() << endl; 
    if(deleteOnShutdown) {
        _ownedObjects.emplace_back(subobject);
    }
}

/******************************************************************************
* Builds a list of degrees of freedom of this object and all its sub-objects.
******************************************************************************/
void FitObject::listAllDOF(std::vector<DegreeOfFreedom*>& list) const
{
    list.insert(list.end(), _dofList.begin(), _dofList.end());
    for(FitObject* subobject : _subobjectList) subobject->listAllDOF(list);
}

/******************************************************************************
* Builds a list of subobjects of this object and all its sub-objects.
******************************************************************************/
void FitObject::listAllSubObject(std::vector<FitObject*>& list) const
{
    list.insert(list.end(), _subobjectList.begin(), _subobjectList.end());
    for(FitObject* subobject : _subobjectList) subobject->listAllSubObject(list);
}

/******************************************************************************
* Returns the degree of freedom with the given ID (and tag).
******************************************************************************/
DegreeOfFreedom* FitObject::DOFById(const FPString& id, const FPString& tag) const
{
    for(DegreeOfFreedom* dof : _dofList) {
        if(dof->id() == id && dof->tag() == tag) return dof;
    }
    return nullptr;
}

/// Recursively searches subobjects to find the the degree of freedom defined as path like str with "." instead of "/"
/// i.e. currentobject.subobject.subsubobject.dofid
DegreeOfFreedom* FitObject::DOFByPath(const FPString& pathId) const 
{
    auto pos = pathId.find(".");
    if(pos!=FPString::npos) {
        FPString childId = pathId.substr(0, pos);
        auto child = childByID(childId);
        return child->DOFByPath(pathId.substr(pos+1));
    }
    return DOFById(pathId);
}

/******************************************************************************
* Builds a list of properties of this object and all its sub-objects.
******************************************************************************/
void FitObject::listAllProperties(vector<FitProperty*>& list) const
{
    boost::insert(list, list.end(), _fitPropertiesList);
    for(FitObject* subobject : _subobjectList) subobject->listAllProperties(list);
}

/******************************************************************************
* Returns subobject with id id.
******************************************************************************/
FitObject* FitObject::childByID(const FPString& id) const
{   
    for(FitObject* f : _subobjectList) {
        if(f->id() == id) return f;
    }
    throw runtime_error(str(format("Could not find subobject %1% in %2%. %2% has %3% subobjects") % id % this->id() % _subobjectList.size()));
}

/******************************************************************************
* Returns the property with the given ID.
******************************************************************************/
FitProperty* FitObject::propertyById(const FPString& id) const
{
    for(FitProperty* prop : _fitPropertiesList) {
        if(prop->id() == id) return prop;
    }
    return nullptr;
}

/******************************************************************************
* Assign absolute weights to the properties.
******************************************************************************/
void FitObject::assignAbsoluteWeights(double absoluteWeight)
{
    // First determine sum of relative weights of the children.
    double relativeWeightSum = 0;
    for(FitObject* object : fitObjects()) {
        if(object->fitEnabled()) {
            relativeWeightSum += object->relativeWeight();
        }
    }

    // Avoid division by zero.
    if(relativeWeightSum == 0) relativeWeightSum = 1;

    // Now calculate absolute weights and assign them to the subObjects.
    for(FitObject* object : fitObjects()) {
        if(object->fitEnabled()) {
            double weight = absoluteWeight * object->relativeWeight() / relativeWeightSum;
            object->assignAbsoluteWeights(weight);
            MsgLogger(debug) << " FitObject::assignAbsoluteWeights()"
                             << " object: " << object->id() << " weight: " << weight << endl;
        }
        else {
            // This will disable all children of the subobject.
            object->assignAbsoluteWeights(0);
        }
    }
}

/******************************************************************************
* Parses the base parameters, common to structures, groups, properties and potentials, from the
* XML element in the job file.
******************************************************************************/
void FitObject::parse(XML::Element element)
{
    if(element.hasAttribute("output")) setOutputEnabled(element.parseBooleanParameterAttribute("output"));
    if(element.hasAttribute("fit")) {
        setFitEnabled(element.parseBooleanParameterAttribute("fit"));
        if(!element.hasAttribute("output")) setOutputEnabled(true);
    }

    setRelativeWeight(element.parseOptionalFloatParameterAttribute("relative-weight", _relativeWeight));
    if(_relativeWeight < 0)
        throw runtime_error(str(format("Relative weight must be non-negative on line %1% of XML file") % element.lineNumber()));
}
}
