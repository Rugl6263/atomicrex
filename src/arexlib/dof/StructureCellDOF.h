///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "DegreeOfFreedom.h"

namespace atomicrex {

class AtomicStructure;

/**
* @brief      This DOF manages the atomic positions in a structure.
*
* @details    It allows to relax these positions to minimize the energy of the structure.
*             Note that this DOF does not store the atomic coordinates. It only provides a
*             facade for the atomic positions stored by the AtomicStructure class.
*/
class StructureCellDOF : public DegreeOfFreedom
{
public:
    /// Mode for structure relaxation
    enum RelaxationMode
    {
        /// Cell dimensions and angles can change
        FULL,
        /// Only allows hydrostatic deformation
        HYDROSTATIC,
        /// Project out the diagonal elements of the virial tensor to allow relaxations at constant
        /// volume, e.g. for mapping out an energy-volume curve. Note: this only approximately conserves the volume and
        /// breaks energy/force consistency so can only be used with optimizers that do require do a line minimisation (e.g. FIRE).
        CONSTANT_VOLUME,
    };

    /// Constructor.
    StructureCellDOF() : DegreeOfFreedom("cell") {}

    /// Returns the number of scalar degrees of freedom this DOF is composed of.
    virtual int numScalars() override;

    /// Lets the DOF export its current value(s) into the given value array.
    ///
    /// \param dst Target array that receives the values of the DOF.
    virtual void exportValue(double*& dst) override;

    /// Lets the DOF import its value(s) from the given value array.
    ///
    /// \param src Source array with values that are imported.
    virtual void importValue(const double*& src) override;

    /// Parse the contents of the <relax> element in the job file.
    ///
    /// \param relaxElement XML Element that contains the options for structure relaxation.
    virtual void parseRelax(XML::Element relaxElement) override;

    /// Lets the DOF write the forces on the cell to the given linear array.
    ///
    /// \param virial Reference to the virial tensor.
    /// \param g_iter Reference to an iterator that stores the gradient of the 
    ///               potential (forces) that are getting minimized.
    void getCellForces(std::array<double, 6>& virial, std::vector<double>::iterator& g_iter);

    /// Lets the DOF write its constraints information to the given linear arrays.
    /// If a scaling factor is set the values are constrained to a range of 
    /// (1 - _scalingFactor)*x0 < x < (1 + _scalingFactor)*x0, otherwise there are
    /// no constraints.
    ///
    /// \param types Vector that receives the type of the constraints of the DOFs.
    /// \param lowerBounds Vector that receives the lower bounds.
    /// \param upperBounds Vector that receives the upper bounds.
    virtual void getBoundConstraints(std::vector<Minimizer::BoundConstraints>::iterator& types,
                                     std::vector<double>::iterator& lowerBounds,
                                     std::vector<double>::iterator& upperBounds) override;

    /// Resets the value of the DOF to what was given in the job file.
    virtual void reset() override;

private:
    /// Returns the structure to which this DOF belongs.
    AtomicStructure* structure() const;

    /// Limits change of lattice constant to (1 - _scalingFactor)*x0 < x < (1 + _scalingFactor)*x0
    double _scalingFactor = 0;

    /// Saves relaxation mode
    RelaxationMode _relaxationMode = FULL;
};

}  // End of namespace
