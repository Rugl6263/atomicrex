///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Potential.h"
#include "functions/Functions.h"

namespace atomicrex {

/************************************************************
 * Class for pair-wise interactions that are concentration
 * dependent.
 *
 * Each interaction (between two species A and B) consists of
 * a potential function V(r) and an interpolation function h(x).
 * The concentration x is computed from the local density
 * of the matrix species S.
 ************************************************************/
class CDIPairInteraction : public FitObject
{
public:
    /// Constructor.
    CDIPairInteraction(const FPString& id, FitJob* job, int atomTypeA, int atomTypeB, int matrix,
                       const std::shared_ptr<FunctionBase>& V, const std::shared_ptr<FunctionBase>& h)
        : FitObject(id, job),
          _atomTypeA(atomTypeA),
          _atomTypeB(atomTypeB),
          _matrix(matrix),
          _V(V),
          _sigmaInverseArgument(new FunctionInverseArgument("CDIPHFunction", job, h))
    {
        registerSubObject(V.get());
        registerSubObject(h.get());
    }

    /// Returns the atom type indices whose interaction is described by this object.
    int atomTypeA() const { return _atomTypeA; }
    int atomTypeB() const { return _atomTypeB; }

    /// Returns the atomic species whose concentration controls the interaction strength.
    int matrix() const { return _matrix; }

    /// Returns whether the interaction between the two given atom types is described by this pair interaction.
    bool interacting(int type_i, int type_j) const
    {
        if(type_i == _atomTypeA && type_j == _atomTypeB) return true;
        if(type_i == _atomTypeB && type_j == _atomTypeA) return true;
        return false;
    }

    /// Returns the pair potential function.
    const std::shared_ptr<FunctionBase>& V() const { return _V; }

    /// Returns the interpolation function.
    const std::shared_ptr<FunctionInverseArgument>& h() const { return _sigmaInverseArgument; }

private:
    /// The two interacting atom types and the third species, whose concentration
    /// controls the interaction strength.
    int _atomTypeA, _atomTypeB, _matrix;

    /// The pair interaction potential.
    std::shared_ptr<FunctionBase> _V;

    /// The interpolation function.
    std::shared_ptr<FunctionInverseArgument> _sigmaInverseArgument;
};

/************************************************************
 * Composition-dependent interatomic potential (CDIP).
 ************************************************************/
class CDIPotential : public Potential
{
public:
    /// Constructor.
    CDIPotential(const FPString& id, FitJob* job) : Potential(id, job, "CDIP"), _linearizedMode(false), _legacyMode(false) {}

    /// Returns the maximum cutoff of the potential.
    virtual double cutoff() const override { return _cutoff; }

    /// Computes the total energy of the structure.
    virtual double computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

    /// Computes the total energy and forces of the structure.
    virtual double computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

    /// This function is called by the fit job on shutdown, i.e. after the fitting process has finished.
    virtual void outputResults() override;

    /// Generates a potential file to be used with simulation codes.
    void writePotential(const FPString& filename) const;

    /// Write potential functions to files for visualization with Gnuplot.
    void writeTables(const FPString& basename) const;

public:
    /// Parses any potential-specific parameters in the XML element in the job file.
    virtual void parse(XML::Element potentialElement) override;

    /// Produces an XML representation of the potential's current parameter values and DOFs that can be used as
    /// input in a subsequent fit job.
    virtual XML::OElement generateXMLDefinition() override;

private:
    /// Parses the <sigmas> element.
    void parseSigmas(XML::Element parentElement);

    /// Parses the <interactions> element.
    void parseInteractions(XML::Element parentElement);

    /// The cutoff of the potential. This is the maximum of all cutoffs of all the sub-functions.
    double _cutoff;

    /// If true, use [h(x_i)+h(x_j)]/2;
    /// otherwise use h([x_i+x_j]/2).
    int _linearizedMode;

    /// If true, compute and use x_i without excluding atom j;
    /// otherwise use x_i(j), i.e. exclude atom j in the calculation of the local concentration that
    /// controls the interaction between the pair i-j.
    int _legacyMode;

    /// Sigma functions. One per atom type; some may be NULL if not handled by the CDIP.
    std::vector<std::shared_ptr<FunctionBase>> _sigmas;

    /// Pair interactions. Number of interaction functions is set by user.
    std::vector<std::shared_ptr<CDIPairInteraction>> _pairInteractions;

    /// Potential file that is written after fitting.
    FPString _exportPotentialFile;

    /// Data file that is written after fitting.
    FPString _exportFunctionsFile;
};

}  // End of namespace
