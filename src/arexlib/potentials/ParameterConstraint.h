///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Atomicrex.h"
#include "../minimizers/Minimizer.h"
#include "../util/xml/XMLUtilities.h"
#include <muParser.h>

namespace atomicrex {

class ScalarDOF;

/**
* @brief Apply constraints to potential parameters
*
* @details    The degrees of freedom of a potential are varied during fitting to minimize
 * the objective function. This allows to couple parameters within different functions using
 * complex epressions.
*/
class ParameterConstraint
{
public:
    ///
    ParameterConstraint() {}

    /// Constructor.
    ParameterConstraint(const FPString& id, FitJob* job) : _id(id), _job(job) {}

    virtual ~ParameterConstraint() = default;

    /// Produces an XML representation. 
    /// XML::OElement generateXMLDefinition() const;

    /// assigns the values stored in _inputDofs to _dofValues
    void updateExpressionVariables();

    void parse(XML::Element element);

    /// update _value
    void compute();
    
    /// Get variable name. If the dofPath alread exists it returns the correct name, else it creates a new one
    /// and registers the input dof.
    FPString getVariableName(FPString& dofPath);

    /// Add dof to input dofs 
    void registerInputDOF(FPString& dofPath);

    /// Return pointer to dependent DOF
    ScalarDOF* dependentDOF() { return _dependantDOF; }

    /// Returns a pointer to the job to which this object belongs.
    FitJob* job() const { return _job; }

private:
    /// Parses the 'expression' expression that links the dof to other dofs
    void parseExpressionElement(XML::Element element);

    /// The expression parser and evaluator.
    mu::Parser _parser;

    /// full path to the dof which depends on the other dofs
    ScalarDOF* _dependantDOF;

    /// lists of properties and dofs that are used to compute this dof when an expression is given
    std::vector<ScalarDOF*> _inputDOFs;

    /// store unique paths to inputs dofs to prevent creation of multiple variables for one dof
    std::vector<FPString> _dofPaths;

    /// stores the current values of the inputs
    std::vector<double> _dofValues;

    /// Pointer to the job this object belongs to.
    FitJob* _job = nullptr;

    /// The identifier string of this object instance.
    FPString _id;
};

}