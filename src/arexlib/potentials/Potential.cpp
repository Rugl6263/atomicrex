///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "Potential.h"
#include "../job/FitJob.h"
#include "../dof/DegreeOfFreedom.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Constructor.
******************************************************************************/
Potential::Potential(const FPString& id, FitJob* job, const FPString& tag) : FitObject(id, job, tag)
{
    // Disable this potential for all atom types by default.
    // It needs to be explicitly enabled for certain types.
    _atomTypeFlags.resize(job->numAtomTypes(), false);

    // Setup N x N interaction table.
    // Disable this potential for all type interactions by default.
    // It needs to be explicitly enabled for certain pairs of types.
    for(int i = 0; i < job->numAtomTypes(); i++) _interactionFlags.push_back(vector<bool>(job->numAtomTypes()));
}

/******************************************************************************
* This function is called by the fit job on shutdown, i.e. after the fitting
* process has finished.
******************************************************************************/
void Potential::outputResults()
{
    if(_outputDefinitionFile.empty() == false) {
        // Build DOM tree in memory.
        XML::OElement rootElement = generateXMLDefinition();
        if(!rootElement)
            throw runtime_error(str(format("Cannot write definition of potential 1%' to XML file. This operation is not "
                                           "supported by the potential class.") %
                                    id()));

        // Create XML document.
        auto doc = XML::make_scoped(xmlNewDoc(BAD_CAST "1.0"), xmlFreeDoc);
        xmlDocSetRootElement(doc.get(), rootElement.release());

        MsgLogger(medium) << "Writing potential definition to " << makePathRelative(_outputDefinitionFile) << endl;

        // Write DOM tree to xml file.
        if(xmlSaveFormatFileEnc(_outputDefinitionFile.c_str(), doc.get(), "UTF-8", 1) <= 0)
            throw runtime_error(str(format("Failed to write XML file: %1%") % _outputDefinitionFile));
    }
}

/******************************************************************************
* Parses the potential element in the job file.
******************************************************************************/
void Potential::parse(XML::Element potentialElement)
{
    // Call base class.
    FitObject::parse(potentialElement);

    // Parse <fit-dof> element.
    XML::Element fitDOFElement = potentialElement.firstChildElement("fit-dof");
    if(fitDOFElement) {
        for(XML::Element dofElement = fitDOFElement.firstChildElement(); dofElement; dofElement = dofElement.nextSibling()) {
            // Lookup referenced degree of freedom.
            FPString dofId = dofElement.tag();
            FPString dofTag = dofElement.parseOptionalStringParameterAttribute("tag");
            DegreeOfFreedom* dof = DOFById(dofId, dofTag);
            if(!dof)
                throw runtime_error(
                    str(format("Invalid element in line %1% of XML file: Unknown degree of freedom \"%2%\" (tag=%3%).") %
                        dofElement.lineNumber() % dofId % (dofTag.empty() ? FPString("<empty>") : dofTag)));

            // Let the DOF parse the rest.
            dof->parseFit(dofElement);
        }
    }

    // Parse output option.
    _outputDefinitionFile = potentialElement.parseOptionalPathParameterElement("export-definition");
}
};
