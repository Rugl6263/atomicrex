///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Potential.h"
#include "functions/Functions.h"

namespace atomicrex {

/************************************************************
 * A pair-wise interaction between two atom types A and B, which
 * is defined by an arbitrary one-dimensional function V_ab(r).
 ************************************************************/
class PairInteraction : public FitObject
{
public:
    /// Constructor.
    PairInteraction(const FPString& id, FitJob* job, int atomTypeA, int atomTypeB, const std::shared_ptr<FunctionBase>& V,
                    const FPString& tag = FPString())
        : FitObject(id, job, tag), _atomTypeA(atomTypeA), _atomTypeB(atomTypeB), _V(V)
    {
        registerSubObject(V.get());
    }

    /// Returns the atom type indices whose interaction is described by this object.
    int atomTypeA() const { return _atomTypeA; }
    int atomTypeB() const { return _atomTypeB; }

    /// Returns whether the interaction between the two given atom types is described by this pair interaction.
    bool interacting(int type_i, int type_j) const
    {
        if(type_i == _atomTypeA && type_j == _atomTypeB) return true;
        if(type_i == _atomTypeB && type_j == _atomTypeA) return true;
        return false;
    }

    /// Returns whether the interaction contains the geiven atom type.
    bool isActive(int type) const
    {
        if(type == _atomTypeA || type == _atomTypeB) return true;
        return false;
    }

    /// Returns the pair potential function.
    const std::shared_ptr<FunctionBase>& V() const { return _V; };

private:
    /// The two interacting atom types and the third species, whose concentration
    /// controls the interaction strength.
    int _atomTypeA, _atomTypeB;

    /// The pair interaction potential.
    std::shared_ptr<FunctionBase> _V;
};

/************************************************************
 * Generic interatomic pair potential.
 ************************************************************/
class PairPotential : public Potential
{
public:
    /// Constructor.
    PairPotential(const FPString& id, FitJob* job) : Potential(id, job, "Pair") {}

    /// Returns the maximum cutoff of the potential.
    virtual double cutoff() const override { return _cutoff; }

    /// Computes the total energy of the structure.
    virtual double computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

    /// Computes the total energy and forces of the structure.
    virtual double computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

    /// This function is called by the fit job on shutdown, i.e. after the fitting process has finished.
    virtual void outputResults() override;

    /// Generates a potential file to be used with simulation codes.
    void writePotential(const FPString& filename) const;

    /// Write potential functions to files for visualization with Gnuplot.
    void writeTables(const FPString& basename) const;

public:
    // Functions that parse XML elements in job file.
    void parse(XML::Element potentialElement) override;
    void parseInteractions(XML::Element parentElement);

    /// Produces an XML representation of the potential's current parameter values and DOFs that can be used as
    /// input in a subsequent fit job.
    virtual XML::OElement generateXMLDefinition() override;

private:
    /// The global cutoff of the potential.
    double _cutoff;

    /// Pair interactions. Number interaction functions is specified by user.
    std::vector<std::shared_ptr<PairInteraction> > _pairInteractions;

    /// Potential file that is written after fitting.
    FPString _exportPotentialFile;

    /// Data file that is written after fitting.
    FPString _exportFunctionsFile;
};

}  // End of namespace
