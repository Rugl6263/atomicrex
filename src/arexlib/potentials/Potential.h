///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Atomicrex.h"
#include "../job/FitObject.h"
#include "../job/FitJob.h"
#include "../structures/AtomicStructure.h"

namespace atomicrex {

class AtomicStructure;  // Declared in AtomicStructure.h
class NeighborList;     // Declared in NeighborList.h
class DegreeOfFreedom;  // Declared in DegreeOfFreedom.h

/**
* @brief      Base class for potentials.
*
* @details    This class is provides methods for calculating the total energy 
*             and the atomic forces for a given structure.
*/
class Potential : public FitObject
{
public:
    /// Constructor.
    Potential(const FPString& id, FitJob* job, const FPString& tag = FPString());

    /// Enables the interaction between two atom types.
    void enableInteraction(int speciesA, int speciesB)
    {
        BOOST_ASSERT(_interactionFlags.size() == job()->numAtomTypes());
        BOOST_ASSERT(speciesA >= 0 && speciesA < _interactionFlags.size());
        BOOST_ASSERT(_interactionFlags[speciesA].size() == job()->numAtomTypes());
        BOOST_ASSERT(speciesB >= 0 && speciesB < _interactionFlags[speciesA].size());
        BOOST_ASSERT(_atomTypeFlags.size() == job()->numAtomTypes());
        _interactionFlags[speciesA][speciesB] = true;
        _interactionFlags[speciesB][speciesA] = true;
        _atomTypeFlags[speciesA] = true;
        _atomTypeFlags[speciesB] = true;
    }

    /// Returns whether the interaction between two atom types is enabled for this potential.
    bool interacting(int speciesA, int speciesB) const
    {
        BOOST_ASSERT(_interactionFlags.size() == job()->numAtomTypes());
        BOOST_ASSERT(speciesA >= 0 && speciesA < _interactionFlags.size());
        BOOST_ASSERT(_interactionFlags[speciesA].size() == job()->numAtomTypes());
        BOOST_ASSERT(speciesB >= 0 && speciesB < _interactionFlags[speciesA].size());
        return _interactionFlags[speciesA][speciesB];
    }

    /// Returns whether the given atom type is handled by this potential.
    /// If not, atoms of this species are completely ignored in the potential routine.
    bool isAtomTypeEnabled(int species) const
    {
        BOOST_ASSERT(_atomTypeFlags.size() == job()->numAtomTypes());
        BOOST_ASSERT(species >= 0 && species < _atomTypeFlags.size());
        return _atomTypeFlags[species];
    }

    /// Returns the maximum cutoff of the potential.
    virtual double cutoff() const = 0;

    /// Computes the total energy and forces of the structure.
    virtual double computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const = 0;

    /// Computes the total energy of the structure.
    virtual double computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const = 0;

    /// This function is called by the fit job on shutdown, i.e. after the fitting process has finished.
    /// Subclasses can override this function to output the fitted potential to a file.
    virtual void outputResults();

	/// Returns the number of bytes of memory per atomic structure requested by the potential.
	/// The system will allocate a memory buffer of the specified size for each atomic structure that
	/// may be used by the potential to store temporary data during energy calculation or permanent 
	/// information associated with each structure.
	virtual size_t perStructureDataSize() const { return 0; }

    /// Returns the number of bytes of working memory per atom required by the potential routine.
    /// The system will provide a memory buffer of the given size for each atomic structure that
    /// can be used by the potential routine.
    virtual size_t perAtomDataSize() const { return 0; }

    /// Returns the number of bytes of working memory per atom neighbor required by the potential routine.
    /// The system will provide a memory buffer of the given size for each atomic structure that
    /// can be used by the potential routine.
    virtual size_t perBondDataSize() const { return 0; }

public:
    /// Parses any potential-specific parameters in the XML element in the job file.
    virtual void parse(XML::Element potentialElement) override;

    /// This virtual method is called by the system before the potential is being used to compute energy and forces.
    /// Note that the method may be called several times.
    /// The default implementation does nothing.
    virtual void preparePotential() {}

    /// Produces an XML representation of the potential's current parameter values and DOFs that can be used as
    /// input in a subsequent fit job.
    virtual XML::OElement generateXMLDefinition() { return XML::OElement(); }

private:
    /// This array controls which atom types are handled by this potential.
    /// Array indices are 0-based!
    std::vector<bool> _atomTypeFlags;

    /// This NxN table controls which interactions between atom types are described by this potential.
    /// Array indices are 0-based!
    std::vector<std::vector<bool>> _interactionFlags;

    /// XML definition file containing the potential's parameters that should be written after fitting.
    FPString _outputDefinitionFile;
};

}  // End of namespace
