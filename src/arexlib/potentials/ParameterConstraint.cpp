///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "ParameterConstraint.h"
#include "../Atomicrex.h"
#include "../job/FitJob.h"
#include "../util/xml/XMLUtilities.h"
#include <muParser.h>

namespace atomicrex {

using namespace std;
using namespace boost;

void ParameterConstraint::parse(XML::Element element) {
    FPString dofPath = element.parseStringParameterAttribute("dependent-dof");
    _dependantDOF = dynamic_cast<ScalarDOF*>(job()->DOFByPath(dofPath));
    if(_dependantDOF->fitEnabled())
        throw runtime_error(
            str(format("dependent-dof %2% in %1% of XML file is not allowed to be enabled for fitting.") %
                    element.lineNumber() % dofPath));
    parseExpressionElement(element.expectChildElement("expression"));
    // Compute here to overwrite the value set when defining the dof before calculating properties the first time
    compute();
}

void ParameterConstraint::parseExpressionElement(XML::Element element) {
    FPString expr = element.textContent();
    // Boundaries of the variables.
    auto start = expr.find('{');
    auto end = expr.find('}');
    int ndof = 0;
    // Go through the equation and deal with all variables
    while(start != FPString::npos) {
        FPString dofPath = expr.substr(start + 1, end - start - 1);
        FPString variableName = getVariableName(dofPath);
        // replace the variables in the equation string.
        expr.replace(start, end - start + 1, variableName);
        // get boundaries of the next variable.
        start = expr.find('{');
        end = expr.find('}');
        }

    _dofValues.resize(_inputDOFs.size());
    for(size_t i = 0; i < _dofValues.size(); i++) {
        _dofValues[i] = *_inputDOFs[i];
        try {_parser.DefineVar(str(format("d_%1%") % i), &_dofValues[i]);}
        catch(mu::Parser::exception_type &e) {
            MsgLogger(medium) << "Exception while defining MuParser variable" << endl;
            MsgLogger(medium) << e.GetMsg() << endl;
            throw runtime_error("Aborting because of previous exception");
        }
    }

    // Feed expression to the parser.
    try {_parser.SetExpr(expr);}
    catch(mu::Parser::exception_type &e) {
            MsgLogger(medium) << "Exception while setting MuParser expression" << endl;
            MsgLogger(medium) << e.GetMsg() << endl;
            throw runtime_error("Aborting because of previous exception");
    }
}

void ParameterConstraint::updateExpressionVariables() {
    for(size_t i = 0; i < _dofValues.size(); i++) {
        _dofValues[i] = *_inputDOFs[i];
        /// Since this is called very often and I am not sure if debugging level is checked everytime is is commented
        ///MsgLogger(debug) << "Updated d_" << i << " to " << *_inputDOFs[i] << " _dofValues" << i << " == " << _dofValues[i] << endl;
    }
}
 
/// Get variable name. If the dofPath alread exists it returns the correct name, else it creates a new one
/// and registers the input dof.
FPString ParameterConstraint::getVariableName(FPString& dofPath)
{
    /// check if dofPath exists in _dofPaths and get its index.
    int i = 0;
    while(i<_dofPaths.size()){
        if(dofPath == _dofPaths[i]) break;
        i++;
    }
    if(i == _dofPaths.size()) registerInputDOF(dofPath);
    return str(format("d_%1%") % i);
}

/// register the dof found using dofPath in the corresponding vectors.
void ParameterConstraint::registerInputDOF(FPString& dofPath)
{   
    _dofPaths.push_back(dofPath);
    ScalarDOF* dof = dynamic_cast<ScalarDOF*>(job()->DOFByPath(dofPath));
    if(dof == nullptr)
        throw runtime_error(
            str(format("Invalid reference in line XML file: Can't find degree of freedom %1%.") % dofPath));
    _inputDOFs.push_back(dof);
    dof-> addChangeListener([this](ScalarDOF& changingDOF) {
        compute();
        });
}

void ParameterConstraint::compute() {
    updateExpressionVariables();
    try{
        auto val = _parser.Eval();
        MsgLogger(debug) << "Computing " << _parser.GetExpr() << " Result: " << val << endl;
        *_dependantDOF=val;
        MsgLogger(debug) << "Set " << _dependantDOF->id() << endl;
    }
    catch(mu::Parser::exception_type &e) {
        MsgLogger(medium) << "Exception while evaluating MuParser expression" << endl;
        MsgLogger(medium) << e.GetMsg() << endl;
        throw runtime_error("Aborting because of previous exception");
    }

}

}