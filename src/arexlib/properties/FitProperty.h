///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Atomicrex.h"
#include "../job/FitObject.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

class ScalarDOF;
class FitJob;  // defined in FitJob.h

/**
 * This base class describes a computed property of an atomic structure (or of a potential).
 *
 * The user can choose whether the property is included in the fit.
 */
class FitProperty : public FitObject
{
public:
    /// Specifies how residual contributions are calculated.
    enum ResidualNorm
    {
        UserDefined,      ///< Indicates that the norm selected by the user should be used.
        Squared,          ///< L2 norm, use squared difference from target value.
        SquaredRelative,  ///< L2 norm, use square of difference from target value divided by target value.
        AbsoluteDiff      ///< L1 norm, absolute value of difference from target value.
    };

public:
    /// Default constructor.
    FitProperty()
    {
        setFitEnabled(false);
        setOutputEnabled(false);
    }

    /// Initialization constructor.
    FitProperty(const FPString& id, const FPString& units, FitJob* job) : FitObject(id, job)
    {
        setFitEnabled(false);
        setOutputEnabled(false);
        initialize(id, units, job);
    }

    /// Initializes the property object. This method must be called if the default constructor was used to create this property.
    void initialize(const FPString& id, const FPString& units, FitJob* job);

    /// Returns the residual of this property used for fitting.
    virtual double residual(ResidualNorm norm) const = 0;

    /// Returns whether this property should be computed because it is included in the fit and/or in the output.
    bool shouldCompute(bool isFitting) const { return (isFitting && _fitEnabled) || (!isFitting && _outputEnabled); }

    /// Outputs the current value of the property.
    virtual void print(MsgLogger& stream) override {}

    /// Lets the property object compute the current value of the property.
    virtual void compute() {}

    /// Recursively assigns absolute weights to the properties.
    void assignAbsoluteWeights(double absoluteWeight) override { _absoluteWeight = absoluteWeight; }

    /// Returns the absolute fit weight assigned to this object.
    double absoluteWeight() const { return _absoluteWeight; }

    /// Returns the tolerance assigned to this fit property.
    double tolerance() const { return _tolerance; }

    /// Sets the tolerance assigned to this fit property.
    void setTolerance(double tolerance) { _tolerance = tolerance; }

    /// Returns how residual contributions are calculated by this property.
    ResidualNorm residualNorm() const { return _residualNorm; }

    /// Returns the units of this property.
    const FPString& units() const { return _units; }

public:
    /// Parse the contents of the <property> element in the job file.
    virtual void parse(XML::Element propertyElement) override;

protected:
    /// The weight factor which is applied to the residual.
    double _absoluteWeight = 1.0;

private:
    /// The tolerance which is used in the calculation of the residual.
    double _tolerance = 1.0;

protected:
    /// Specifies how residual contributions are calculated.
    ResidualNorm _residualNorm = Squared;

    /// The units of this property.
    FPString _units;

    friend class FitObject;
};

/**
 * This is a simple property having a scalar value.
 *
 * The user can specify a scalar target value for the property.
 */
class ScalarFitProperty : public FitProperty
{
public:
    /// Default constructor.
    ScalarFitProperty() : _computedValue(0), _targetValue(0) {}

    /// Initialization constructor.
    ScalarFitProperty(const FPString& id, const FPString& units, FitJob* job)
        : FitProperty(id, units, job), _computedValue(0), _targetValue(0)
    {
    }

    /// Initializes the property object. This method must be called if the default constructor has been used.
    void initialize(const FPString& id, const FPString& units, FitJob* job) { FitProperty::initialize(id, units, job); }

    /// Returns the residual of this property used for fitting.
    virtual double residual(ResidualNorm norm) const override;

    /// Returns the current computed value of the property.
    operator double() const { return _computedValue; }

    /// Getter for _computedValue since double() operator was hard to interface
    double computedValue() const { return _computedValue; }
    /// Assigns a new value to the property.
    ScalarFitProperty& operator=(double value)
    {
        _computedValue = value;
        return *this;
    }

    /// Outputs the current value of the property.
    virtual void print(MsgLogger& stream) override;

public:
    /// Parse the contents of the <property> element in the job file.
    virtual void parse(XML::Element propertyElement) override;

    /// Returns target value (scalar)
    double targetValue() { return _targetValue; }

    /// Sets target value
    void setTargetValue(double targetValue) { _targetValue = targetValue; }

protected:
    /// The computed value of the property.
    double _computedValue;

    /// The target value for the property.
    double _targetValue;
};

/**
 * This type of scalar property is directly coupled to a degree of freedom of a structure.
 *
 * This only makes real sense if the corresponding DOF is being relaxed. That is, if the actual value of the DOF
 * differs from what is being set by the user (or the minimizer routine).
 *
 * If the coupled property is set to be computed, then the corresponding degree of freedom is being relaxed, and
 * the value of the degree of freedom after relaxation becomes the value of the property.
 */
class CoupledFitProperty : public ScalarFitProperty
{
public:
    /// Constructor.
    CoupledFitProperty(ScalarDOF& dof, const FPString& units, FitJob* job);

    /// Returns the degree of freedom to which this property is coupled.
    ScalarDOF& dof() const { return _dof; }

    /// Lets the property object compute the current value of the property.
    virtual void compute() override;

    /// Outputs the current value of the property.
    virtual void print(MsgLogger& stream) override;

public:
    /// Parse the contents of the <property> element in the job file.
    virtual void parse(XML::Element propertyElement) override;

private:
    /// The degree of freedom to which this property is coupled.
    ScalarDOF& _dof;
};

}  // End of namespace
