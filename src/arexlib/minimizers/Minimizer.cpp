///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "Minimizer.h"
#include "LBFGSMinimizer.h"
#include "SplitBregmanMinimizer.h"
#include "SpaMinimizer.h"
#ifdef ATOMICREX_USE_NLOPT
#include "NloptMinimizer.h"
#endif

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Constructor that copies another minimizer
******************************************************************************/
Minimizer::Minimizer(const Minimizer& other)
    : _convergenceThreshold(other._convergenceThreshold),
      _maximumNumberOfIterations(other._maximumNumberOfIterations),
      _gradientEpsilon(other._gradientEpsilon),
      _job(other._job),
      _isFitMinimizer(other._isFitMinimizer),
      _name(other._name)
{
}

/******************************************************************************
* Print the current status to command line
******************************************************************************/
void Minimizer::printStep(double residual)
{
    if(_isFitMinimizer) {
        MsgLogger(medium) << "  " << _name << ": iter= " << itercount() << " residual= " << residual
                          << " grad= " << gradientNorm2() << " opt " << _bestResidual << endl;

        if(_intervalForPrinting > 0 && (itercount() % _intervalForPrinting) == 0) _job->printPotentialDOFList(false);
    }
}

/******************************************************************************
* Computes the gradient of the objective function using finite differences.
******************************************************************************/
double Minimizer::numericGradient(const vector<double>& x, vector<double>& g)
{
    BOOST_ASSERT(_func);

    vector<double>& nonconst_x = const_cast<vector<double>&>(x);

    vector<double>::iterator g_iter = g.begin();
    vector<double>::iterator x_iter = nonconst_x.begin();
    for(; x_iter != nonconst_x.end(); ++x_iter, ++g_iter) {
        double oldx = *x_iter;
        *x_iter = oldx + _gradientEpsilon;
        double e_plus = _func(nonconst_x);
        *x_iter = oldx - _gradientEpsilon;
        double e_minus = _func(nonconst_x);
        *x_iter = oldx;
        *g_iter = (e_plus - e_minus) / (2.0 * _gradientEpsilon);
    }

    return _func(x);
}

/******************************************************************************
* Initializes the minimizer.
* Must be called once before entering the minimization loop.
******************************************************************************/
void Minimizer::prepare(std::vector<double>&& x0, const std::function<double(const std::vector<double>&)>& func,
                        const std::function<double(const std::vector<double>&, std::vector<double>&)>& gradient)
{
    _func = func;
    _gradientFunc = gradient;
    f = 0.0;
    _gradNorm2 = 0.0;
    _itercount = 0;

    _bestResidual = HUGE_VAL;
    _bestParameters = x0;
}

/******************************************************************************
* Parses the minimizer's parameters from the XML file.
******************************************************************************/
void Minimizer::parse(XML::Element minimizerElement)
{
    MsgLogger(debug) << "Parse minimizer parameters." << endl;
    _convergenceThreshold = minimizerElement.parseOptionalFloatParameterAttribute("conv-threshold", _convergenceThreshold);
    _maximumNumberOfIterations = minimizerElement.parseOptionalIntParameterAttribute("max-iter", _maximumNumberOfIterations);
    _gradientEpsilon = minimizerElement.parseOptionalFloatParameterAttribute("gradient-epsilon", _gradientEpsilon);
}

/******************************************************************************
* Factory function that creates an instance of the minimizer type that is
* defined in the XML file.
******************************************************************************/
std::unique_ptr<Minimizer> Minimizer::createAndParse(XML::Element minimizerElement, FitJob* job, int intervalForPrinting,
                                                     bool isFitMinimizer)
{
    std::unique_ptr<Minimizer> minimizer;

    // Create an instance of the selected minimizer class.
    if(minimizerElement.tagEquals("BFGS"))
        minimizer = std::unique_ptr<LBFGSMinimizer>(new LBFGSMinimizer(job, intervalForPrinting, isFitMinimizer));
    else if(minimizerElement.tagEquals("split-bregman"))
        minimizer = std::unique_ptr<SplitBregmanMinimizer>(new SplitBregmanMinimizer(job, intervalForPrinting, isFitMinimizer));
    else if(minimizerElement.tagEquals("spa"))
        minimizer = std::unique_ptr<SpaMinimizer>(new SpaMinimizer(job, intervalForPrinting, isFitMinimizer));
    else if(minimizerElement.tagEquals("nlopt"))
#ifdef ATOMICREX_USE_NLOPT
        minimizer = std::unique_ptr<NloptMinimizer>(new NloptMinimizer(job, intervalForPrinting, isFitMinimizer));
#else
        throw runtime_error(str(
            format("Program has been built without NLopt support. Unsupported minimizer type <%1%> in line %2% of XML file.") %
            minimizerElement.tag() % minimizerElement.lineNumber()));
#endif
    else
        throw runtime_error(str(format("Unknown minimizer type <%1%> in line %2% of XML file.") % minimizerElement.tag() %
                                minimizerElement.lineNumber()));

    // Let the minimizer parse its parameters.
    minimizer->parse(minimizerElement);

    return minimizer;
}
}
