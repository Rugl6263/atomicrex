///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "SplitBregmanMinimizer.h"
#include "../job/FitJob.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Constructor that copies another minimizer
******************************************************************************/
SplitBregmanMinimizer::SplitBregmanMinimizer(const SplitBregmanMinimizer& other) : Minimizer(other)
{
    _lambda = other._lambda;
    _mu = other._mu;
    _submin = other._submin->clone();
}

/******************************************************************************
* Initializes the minimizer.
******************************************************************************/
void SplitBregmanMinimizer::prepare(std::vector<double>&& x0, const std::function<double(const std::vector<double>&)>& func,
                                    const std::function<double(const std::vector<double>&, std::vector<double>&)>& gradient)
{
    Minimizer::prepare(std::move(x0), func, gradient);

    _d = std::vector<double>(x0.size(), 0.0);
    _b = std::vector<double>(x0.size(), 0.0);

    BOOST_ASSERT_MSG(_submin, "Split Bregman needs a secondary minimizer.");
}

/******************************************************************************
* Sets constraints for the variation of the parameters.
******************************************************************************/
void SplitBregmanMinimizer::setConstraints(std::vector<BoundConstraints>&& constraintTypes, std::vector<double>&& lowerBounds,
                                           std::vector<double>&& upperBounds)
{
    BOOST_ASSERT(lowerBounds.size() == _d.size());
    BOOST_ASSERT(upperBounds.size() == _d.size());
    BOOST_ASSERT(constraintTypes.size() == _d.size());

    BOOST_ASSERT(_submin);
    _submin->setConstraints(std::move(constraintTypes), std::move(lowerBounds), std::move(upperBounds));
}

/******************************************************************************
* Parses the minimizer's parameters from the XML file.
******************************************************************************/
void SplitBregmanMinimizer::parse(XML::Element minimizerElement)
{
    Minimizer::parse(minimizerElement);

    _lambda = minimizerElement.parseOptionalFloatParameterAttribute("lambda", _lambda);
    _mu = minimizerElement.parseOptionalFloatParameterAttribute("mu", _mu);

    XML::Element sub = minimizerElement.firstChildElement();
    if(!sub)
        throw runtime_error(str(format("<%1%> element in line %2% of XML file must contain a sub-minimizer.") %
                                minimizerElement.tag() % minimizerElement.lineNumber()));

    _submin = createAndParse(sub, job());
}

/******************************************************************************
* Do one iteration of Split Bregman. Internally uses BFGS. If BFGS results
* ABNORMAL, so does this. Returns SplitBregmanResult.
******************************************************************************/
Minimizer::MinimizerResult SplitBregmanMinimizer::iterate()
{
    // Fetch x0.
    vector<double> xOld(job()->numActiveDOF());
    job()->packDOF(xOld);

    // Computes the value of the function 0.5||Ax-f||^2 + lambda*0.5*||d-mu*x_b||^2,
    // as defined in eq. 18 in "Compressive sensing as a new paradigm for model
    // building", L.J. Nelson, G.L.W. Hart.
    auto sbfunc = [this](const vector<double>& x) -> double {
        // Compute ||Ax-f||^2
        job()->unpackDOF(x);
        double term1 = job()->calculateResidual();

        // Compute ||d - mu*x - b||^2;
        auto x_iter = x.begin();
        auto d_iter = _d.begin();
        auto b_iter = _b.begin();
        double term2squared = 0.0;
        for(; x_iter != x.end(); x_iter++, d_iter++, b_iter++) {
            term2squared += square(*d_iter - _mu * (*x_iter) - *b_iter);
        }

        return 0.5 * term1 + 0.5 * _lambda * term2squared;
    };

    // Initialize internal minimizer (e.g. BFGS).
    // It's important take pass a copy of xOld to this function, because it likely destroys the vector's contents.
    _submin->prepare(vector<double>(xOld), sbfunc);

    // Minimization loop.
    MinimizerResult result;
    while((result = _submin->iterate()) == MINIMIZATION_CONTINUE &&
          _submin->itercount() <= _submin->maximumNumberOfIterations()) {
        MsgLogger(debug) << "  iter= " << _submin->itercount() << " residual= " << _submin->value()
                         << " grad= " << _submin->gradientNorm2() << endl;

        // Check if stop file has been created.
        ifstream stopFile("stop_fitpot");
        if(stopFile.is_open()) {
            MsgLogger(minimum) << "Found 'stop_fitpot' file. Minimization will be terminated." << endl;
            break;
        }
    }
    if(result == MINIMIZATION_ERROR)
        throw runtime_error("The minimizer failed with an error while minimizing the objective function.");

    // Pack new dofs into xNew.
    vector<double> xNew(job()->numActiveDOF());
    job()->packDOF(xNew);

    shrinkAll(xNew);
    doArithmetic(xNew);

    _diff = computeDiff(xOld, xNew, 1);
    _l1norm = computeNorm(xNew, 1);
    ++_itercount;

    // Output status information.
    MsgLogger(minimum) << "  Split Bregman iter = " << itercount() << "   |new-old| = " << diff() << "   |x| = " << xNorm()
                       << "   |Ax-b|l1 = " << job()->calculateResidual(FitProperty::AbsoluteDiff)
                       << "   |Ax-b|l2 = " << job()->calculateResidual(FitProperty::Squared) << endl;

    if(_diff <= convergenceThreshold() && (result == MINIMIZATION_CONTINUE || result == MINIMIZATION_CONVERGED))
        return MINIMIZATION_CONVERGED;
    else if(result == MINIMIZATION_CONTINUE || result == MINIMIZATION_CONVERGED)
        return MINIMIZATION_CONTINUE;
    else
        return MINIMIZATION_ABNORMAL;
}

/******************************************************************************
* Computes the norm, with respect to l1 or l2 norm of two vectors.
* To be used when figuring out if Split Bregman has converged for example.
* Returns 0.0 if invalid norm was specified.
******************************************************************************/
double SplitBregmanMinimizer::computeNorm(const vector<double>& x, int whichNorm)
{
    // Compute l1 or l2 norm of x1-x2.
    if(whichNorm == 1) {
        double ret = 0.0;
        for(double v : x) ret += fabs(v);
        return ret;
    }
    else if(whichNorm == 2) {
        double ret = 0.0;
        for(double v : x) ret += square(v);
        return sqrt(ret);
    }
    else {
        return 0.0;
    }
}

/******************************************************************************
* Computes the difference, with respect to l1 or l2 norm of two vectors.
* To be used when figuring out if Split Bregman has converged for example.
* Returns 0.0 if invalid norm was specified.
******************************************************************************/
double SplitBregmanMinimizer::computeDiff(const vector<double>& x1, const vector<double>& x2, int whichNorm)
{
    // Compute l1 or l2 norm of x1-x2.
    if(whichNorm == 1) {
        double ret = 0.0;
        auto x1_iter = x1.begin();
        auto x2_iter = x2.begin();
        for(; x1_iter != x1.end(); x1_iter++, x2_iter++) {
            ret += fabs(*x1_iter - *x2_iter);
        }
        return ret;
    }
    else if(whichNorm == 2) {
        double ret = 0.0;
        auto x1_iter = x1.begin();
        auto x2_iter = x2.begin();
        for(; x1_iter != x1.end(); x1_iter++, x2_iter++) {
            ret += square(*x1_iter - *x2_iter);
        }
        return sqrt(ret);
    }
    else {
        return 0.0;
    }
}

/******************************************************************************
* Does the simple arithmetic step of SplitBregman. Eq. 20 in "Compressive
* sensing as a new paradigm for model building", L.J. Nelson, G.L.W. Hart.
******************************************************************************/
void SplitBregmanMinimizer::doArithmetic(const vector<double>& x)
{
    auto d_iter = _d.begin();
    auto b_iter = _b.begin();
    auto x_iter = x.begin();
    for(; x_iter != x.end(); x_iter++, d_iter++, b_iter++) {
        *b_iter = *b_iter + _mu * (*x_iter) - *d_iter;
    }
}

/******************************************************************************
* Does the shrinking step of SplitBregman. Eq. 19 in "Compressive
* sensing as a new paradigm for model building", L.J. Nelson, G.L.W. Hart.
******************************************************************************/
void SplitBregmanMinimizer::shrinkAll(const vector<double>& x)
{
    // Shrink operator as defined by eq. 11, page 5 in "Compressive sensing as
    // a new paradigm for model building", L.J. Nelson, G.L.W. Hart.
    // Uses copysign() from <cmath> (doesn't work quite right for 0, but for shrink
    // that doesn't matter since the result will be 0 anyway.)
    auto shrink = [](double y, double alpha) { return copysign(1.0, y) * fmax(fabs(y) - alpha, 0.0); };

    auto d_iter = _d.begin();
    auto b_iter = _b.begin();
    auto x_iter = x.begin();
    for(; x_iter != x.end(); x_iter++, d_iter++, b_iter++) {
        *d_iter = shrink(_mu * (*x_iter) + *b_iter, 1.0 / _lambda);
    }
}

/******************************************************************************
* Function that creates a copy of this minimizer.
******************************************************************************/
std::unique_ptr<Minimizer> SplitBregmanMinimizer::clone()
{
    // return std::make_shared<SplitBregmanMinimizer>(*this);
    return std::unique_ptr<SplitBregmanMinimizer>(new SplitBregmanMinimizer(*this));
}

}  // End of namespace.
