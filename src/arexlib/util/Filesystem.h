///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Atomicrex.h"

namespace atomicrex {

#ifndef USE_BOOST_FILESYSTEM_LIB
namespace fs = std::experimental::filesystem;
#else
namespace fs = boost::filesystem;
#endif

/// Given an absolute input path 'path', and an absolute base path 'relative_to',
/// returns a relative path that points to the same file/directory as 'path'.
inline FPString makePathRelative(const FPString& path, const FPString& relative_to)
{
    // Create absolute paths
    fs::path p = fs::absolute(path);
    fs::path r = fs::absolute(relative_to);

    // if root paths are different, return absolute path
    if(p.root_path() != r.root_path()) return p.native();

    // initialize relative path
    fs::path result;

    // find out where the two paths diverge
    fs::path::const_iterator itr_path = p.begin();
    fs::path::const_iterator itr_relative_to = r.begin();
    while(itr_path != p.end() && itr_relative_to != r.end() && *itr_path == *itr_relative_to) {
        ++itr_path;
        ++itr_relative_to;
    }

    // add "../" for each remaining token in relative_to
    if(itr_relative_to != r.end()) {
        ++itr_relative_to;
        while(itr_relative_to != r.end()) {
            result /= "..";
            ++itr_relative_to;
        }
    }

    // add remaining path
    while(itr_path != p.end()) {
        result /= *itr_path;
        ++itr_path;
    }

    return result.native();
}

/// Given an absolute input path 'path',
/// returns a path relative to the current working directory that points to the same file/directory as 'path'.
inline FPString makePathRelative(const FPString& path) { return makePathRelative(path, fs::current_path().native()); }
}