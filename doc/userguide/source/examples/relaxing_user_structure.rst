.. _example_relaxing_user_structure:

.. index::
   single: Analytic bond-order potential (ABOP); example
   single: Examples; Analytic bond-order potential (ABOP)
   single: Examples; User defined structures
   single: User defined structures; example
   single: Examples; Multi-component system

Relaxation of a user defined structure
=================================================================================================

This example demonstrates the definition and relaxation of a
:ref:`user defined structure provided in the input file
<structure_in_input_file>` via the `<user-structure>` element. None of
the parameters are fitted.

Location
------------------

`examples/relaxing_user_structure`

Input files
------------------

* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/relaxing_user_structure/main.xml
       :linenos:
       :language: xml

* `GaN.tersoff`: initial parameter set in `Lammps/Tersoff format
  <http://lammps.sandia.gov/doc/pair_tersoff.html>`_

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/relaxing_user_structure/GaN.tersoff
       :linenos:
       :language: text


Output
------------------

* The final properties (as well as parameters) are written to standard
  output.

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/relaxing_user_structure/reference_output/log
       :linenos:
       :language: text
