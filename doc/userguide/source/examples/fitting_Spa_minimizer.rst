.. _example_fitting_spa_minimizer:

.. index::
   single: Spa minimizer; example
   single: Examples; Spa minimizer


Fitting using the Spa minimizer
==============================================

This example demonstrates the use of the :ref:`Spa minimizer <spa>`.
The example employs a simple embedded atom method potential (EAM) and
a structure database that is also employed in another :ref:`example
<example_fitting_eam_potential>`. When using the Spa minimizer it
is strongly recommended to :ref:`impose bounds on parameters and
structural properties <spa_bounds>`.

.. note::

   Since the Spa minimizer employs a `pseudorandom number generator
   <https://en.wikipedia.org/wiki/Pseudorandom_number_generator>`_ the
   *exact* outcome of the minimization procedure can differ when run
   on *different* machines.

Location
------------------

`examples/fitting_Spa_minimizer`

Input files
------------------

* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_Spa_minimizer/main.xml
       :linenos:
       :language: xml

* `potential.xml`: initial parameter set (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/fitting_Spa_minimizer/potential.xml
       :linenos:
       :language: xml

* `structures.xml`: input structures (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/fitting_Spa_minimizer/structures.xml
       :linenos:
       :language: xml

* `POSCAR_res_POSCAR_0.9.traj_forces`: file with input configuration
  and forces

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_Spa_minimizer/POSCAR_res_POSCAR_0.9.traj_forces
       :linenos:
       :language: text

* `POSCAR_res_POSCAR_1.0.traj_forces`: file with input configuration
  and forces

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_Spa_minimizer/POSCAR_res_POSCAR_1.0.traj_forces
       :linenos:
       :language: text

* `POSCAR_res_POSCAR_1.1.traj_forces`: file with input configuration
  and forces

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_Spa_minimizer/POSCAR_res_POSCAR_1.1.traj_forces
       :linenos:
       :language: text


Output (files)
------------------

* The final properties (as well as parameters) are written to standard
  output.

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_Spa_minimizer/reference_output/log
       :linenos:
       :language: text
