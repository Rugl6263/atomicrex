.. _example_potential_eam:

.. index::
   single: Embedded atom method (EAM); example
   single: Examples; Embedded atom method (EAM)
   single: Examples; User defined functions
   single: User defined functions; example
   single: Examples; extended markup language (XML) inclusions
   single: extended markup language (XML); Inclusions; example


Embedded atom method (EAM) potential with user defined functions
=================================================================================================

This example demonstrates the use of the :ref:`embedded atom method
(EAM) potential <eam_potential>` routine together with :ref:`user
defined functions <user_defined_functions>`. The potential form and
parameters have been taken from [MisMehPap01]_. The example also
illustrates the use of :ref:`XML Inclusions <xml_inclusions>`. Note
that the potential is merely evaluated for a couple of simple lattice
structures and none of the parameters are fitted.

Location
------------------

`examples/potential_EAM`

Input files
------------------

* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/potential_EAM/main.xml
       :linenos:
       :language: xml

* `potential.xml`: initial parameter set (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/potential_EAM/potential.xml
       :linenos:
       :language: xml

* `structures.xml`: input structures (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/potential_EAM/structures.xml
       :linenos:
       :language: xml


Output (files)
------------------

* The final properties (as well as parameters) are written to standard
  output.

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/potential_EAM/reference_output/log
       :linenos:
       :language: text
