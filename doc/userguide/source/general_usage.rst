General usage
***************

Running atomicrex
====================


The syntax used to run :program:`atomicrex` takes the following
simple form::

   $ ./atomicrex <jobfile>

The ``<jobfile>`` argument represents an XML job file that is
processed by :program:`atomicrex`.

.. _jobfile:

.. index::
   single: Job file
   single: extended markup language (XML)
   single: <job>

The job file
====================

A job file controls the operation of the :program:`atomicrex`
program. It defines the potential to be fitted and the atomic
structures and their properties which are used for the fitting
process.

The format of a job file is a custom `extended markup language (XML)
<https://en.wikipedia.org/wiki/XML>`_ dialect, which is described in
the following. The general structure of a job file is::

    <?xml version="1.0" encoding="iso-8859-1"?>
    <job>
      <!-- general settings -->
      <name>...</name>
      <verbosity>...</verbosity>
      <real-precision>...</real-precision>
      <validate-potentials />

      <!-- specify the different atom types (to be referred to in the blocks that follow) -->
      <atom-types>
        ...
      </atom-types>

      <!-- specify optimization procedure -->
      <fitting>
        ...
      </fitting>

      <!-- specify potential form and parameters -->
      <potentials>
        ...
      </potentials>

      <!-- specify structures and associated properties -->
      <structures>
        ...
      </structures>
    </job>


Following the XML header, the root element ``<job>`` contains several
tags and sections.

.. index::
   single: Job name
   single: Input variables; Job name
   single: Verbosity
   single: Input variables; Verbosity
   single: Input variables; Floating point precision
   single: <name>
   single: <verbosity>
   single: <real-precision>
   single: Debugging and testing; Validation of potentials
   single: <validate-potentials>

First, there are several "general" settings.

* The optional ``<name>`` block specifies the name of the job.
* The verbosity of the output is controlled using the ``<verbosity>``
  tag, which supports the settings ``none``, ``minimum``, ``medium``,
  ``maximum``, and ``debug``.
* The precision of the floating point input/output is controlled via
  the ``<real-precision>`` tag, which expects an integer argument.
* The ``<validate-potentials>`` tag serves a special purpose and is
  usually only used during debugging and error tracing. It enforces
  the numerical evaluation of the forces and compares the outcome with
  the results from the force calculation implemented in the potential
  source code. This enables one to check the correctness of the latter
  implementation. As the numerical calculation is, however, *much*
  slower, this tag should *not* be activated deliberately.

.. _atom_types:

.. index::
   single: Atom types
   single: Input variables; Atom types
   single: <atom-types>

Then follows the ``<atom-types>`` block, in which the user has to
specify the different atom types using the following format::

        <atom-types>
          <species mass='69.723' atomic-number='31'>Ga</species>
          <species mass='22.989' atomic-number='11'>N</species>
        </atom-types>

The ``mass`` and ``atomic-number`` attributes are optional. They are
currently only needed when writing a fitted
:ref:`embedded atom method (EAM) potential <eam_potential>` to a tabulated
EAM potential file.

.. index::
   single: <fitting>
   single: <potentials>
   single: <structures>
   single: extended markup language (XML); Inclusions

.. _xml_inclusions:

The bulk of the input parameters is then contained in the three final
sections, which define the potentials that are subject to the fitting,
the structures that are included in the fit, and general parameters
that control the fitting process.  The optimization procedure is
specified in the ``<fitting>`` block, which is explained in :ref:`this
section <training>`.  The ``<potentials>`` and ``<structures>`` blocks
are described in the sections on :ref:`potentials <potentials>` and
:ref:`structures <structures>`.  Note that the input file format
supports `XML Inclusions (XInclude)
<https://de.wikipedia.org/wiki/XInclude>`_, which provides an elegant
mechanism for structuring the input file and reusing blocks.
