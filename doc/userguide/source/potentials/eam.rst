.. _eam_potential:
.. index::
   single: Embedded atom method (EAM); description
   single: Embedded atom method (EAM); parameters
   single: Pair functionals; Embedded atom method (EAM)
   single: Two-body potentials; Embedded atom method (EAM)
   single: <eam>

Embedded atom method potentials
=======================================

There is a host of different embedding type potential formats
including the embedded atom method (EAM) [DawBas84]_, the glue
potential [ErcTosPar86]_, the effective medium theory (EMT)
[PusNieMan81]_, [Nor82]_, and the Finnis-Sinclair (FS) form
[FinSin84]_. The total energy for these *pair functionals* can be
written in the general form

.. math::
  E = \sum_{ij} V(r_{ij}) + \sum_i F(\rho_i)

where

.. math::
   \rho_i = \sum_j \rho(r_{ij}).

The various original formats differ with respect to the functional
forms chosen for :math:`V`, :math:`F`, and :math:`\rho`. In the
present implementation it is possible to :ref:`compose functional
forms <user_defined_functions>` using a math parser. This allows one
not only to replicate any of the original forms but to define
practically arbitrary functional forms. This is demonstrated by the
:ref:`construction of an EAM potential with user defined functions
<example_potential_eam>`, which employs the
functional form from [MisMehPap01]_, as well as in the example, in
which :ref:`a simple EAM potential for aluminum is fitted
<example_fitting_EAM_potential>`.

.. index::
   single: Pair potentials; general pair potential
   single: Two-body potentials; general pair potential

.. note::

   By omitting the embedding term, the EAM potential form can be used
   to fit arbitrary pair potentials.

The following code block illustrates the definition of a rather simple
functional form.  The two main subelements of the ``<eam>`` block are
``<mapping>`` and ``<functions>``.  The ``<functions>`` block
comprises the definitions of the various functions and parameters,
which is described in detail in the section on the `specification of
functional forms <function_definition>`. Each function defined has to
be assigned an id using the `id` attribute. The id is used in the
``<mapping>`` block to attach the functions to certain types of
interaction (pair potential ``V`` and electron density ``rho``) or
atom type (embedding function ``F``).

.. code:: xml

   <eam id="Al" species-a="*" species-b="*">
     <export-eam-file resolution='500' rho-range-factor='2.0'>output.eam.fs</export-eam-file>

     <mapping>
       <pair-interaction species-a="*" species-b="*" function="V" />
       <electron-density species-a="*" species-b="*" function="rho" />
       <embedding-energy species="*" function="F" />
     </mapping>

     <functions>
       <user-function id="V">
         <input-var>r</input-var>
         <expression> A*exp(-lambda*r) </expression>
         <derivative> -lambda*A*exp(-lambda*r) </derivative>
         <param name="A">500</param>
         <param name="lambda">2.73</param>
         <fit-dof> <A/> <lambda/> </fit-dof>
         <screening>
           <user-function id="V_screening">
             <cutoff>6.5</cutoff>
             <input-var>r</input-var>
             <expression> 1 - 1/(1 + ((r - cutoff) / h)^4) </expression>
             <derivative> 4 * h^4 * (r - cutoff)^3 / ((h^4 + (r - cutoff)^4)^2) </derivative>
             <param name="h">3</param>
           </user-function>
         </screening>
       </user-function>

       <user-function id="rho">
         <input-var>r</input-var>
         <expression> exp(-twomu*r) </expression>
         <derivative> -twomu*exp(-twomu*r) </derivative>
         <param name="twomu">1.14</param>
         <fit-dof> <twomu/> </fit-dof>
         <screening>
           <user-function id="rho_screening">
             <cutoff>6.5</cutoff>
             <input-var>r</input-var>
             <expression> 1 - 1/(1 + ((r - cutoff) / h)^4) </expression>
             <derivative> 4 * h^4 * (r - cutoff)^3 / ((h^4 + (r - cutoff)^4)^2) </derivative>
             <param name="h">3</param>
           </user-function>
         </screening>
       </user-function>

       <user-function id="F">
         <input-var>rho</input-var>
         <expression> -D*sqrt(rho) </expression>
         <derivative> -D/(2*sqrt(rho)) </derivative>
         <param name="D">8</param>
         <fit-dof> <D/> </fit-dof>
       </user-function>

     </functions>
   </eam>



.. rubric:: Elements and attributes

.. index:: <mapping>, <pair-interaction>, <electron-density>,
           <embedding-energy>

* ``<mapping>``: This block defines the mapping of the functions defined
  in the ``<functions>`` block onto different atom types and pairs of
  atom types.

  * ``<pair-interaction>``: Assign a pair interaction using the
    attributes ``species-a`` and ``species-b`` to specify the atom types
    involved and the ``function`` attribute to specify the id of the
    function. The function id has to match exactly one of the
    functions defined in the ``<functions>`` block.
  * ``<electron-density>``: Assign an electron density function using
    the attributes ``species-a`` and ``species-b`` to specify the atom
    types involved and the ``function`` attribute to specify the id of
    the function. The function id has to match exactly one of the
    functions defined in the ``<functions>`` block.
  * ``<embedding-energy>``: Assign an embedding function using the
    attribute ``species`` to specify the atom type involved and the
    ``function`` attribute to specify the id of the function. The
    function id has to match exactly one of the functions defined in
    the ``<functions>`` block.

.. index:: <functions>, <export-functions>

* ``<functions>``: This block comprises the definitions of the various
  functions and parameters. The definition of a function is described
  :ref:`here <function_definition>`. Each function defined has to be
  assigned an id using the ``id`` attribute. The id is used in the
  ``<mapping>`` block to attach a function to a certain type of
  interaction (pair potential and electron density) or atom type
  (embedding function).

* ``<export-eam-file>`` (optional): Name of file, to which the tabulated
  functions of the EAM potential are being written. The resulting file is
  in the Lammps 'eam/fs' format and will include tabulated function data for all
  elements which have at least an embedding function defined and their
  pairwise combinations. The resolution of the generated function tables
  can be adjusted with the optional ``resolution`` attribute (default value: 500).
  The range of the embedding energy function to be tabulated and exported
  is automatically determined by finding the maximum electron density
  occurring in any of the structures of the current fit job. This maximum
  electron density is multiplied by an additional margin factor to determine
  the export range. The factor, which is 2.0 by default, can be overwritten
  by specifying the optional ``rho-range-factor`` attribute.
  Note that masses and atomic numbers, which are written to the EAM file,
  must be specified in the ``<species>`` elements of the job's ``<atom-types>`` list.
  [Default: no file is written].


.. rubric:: Further information

More detailed information can be found in [StoZar80]_, [PusNieMan81]_,
[Nor82]_, [FinSin84]_, [DawBas83]_, [DawBas84]_, [FoiBasDaw86]_,
[DawFoiBas93]_, [ErcTosPar86]_, [MisMehPap01]_.
