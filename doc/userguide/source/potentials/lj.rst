.. _lennard_jones_potential:
.. index::
   single: Lennard-Jones potential; description
   single: Lennard-Jones potential; parameters
   single: Pair potentials; Lennard-Jones potential
   single: Two-body potentials; Lennard-Jones potential
   single: <lennard-jones>

Lennard-Jones potential
===============================

The Lennard-Jones potential has the following functional form

.. math::

     E = \sum_{ij} V(r_{ij})

where

.. math::

     V(r)
     = 4 \varepsilon \left[ \left(\frac{\sigma}{r}\right)^{12}
     - \left(\frac{\sigma}{r}\right)^{6} \right].

Here, :math:`\varepsilon` and :math:`\sigma` are the two potential
parameters that, respectively, define energy and length scale. The
potential is truncated beyond a certain cutoff.

The following code snippet, to be inserted in the ``<potentials>``
block of the input file illustrates the definition of this potential
type in the input file.

.. code:: xml

   <lennard-jones id="Lennard-Jones" species-a="*" species-b="*">
     <sigma>2.5</sigma>
     <epsilon>0.5</epsilon>
     <cutoff>10.0</cutoff>
     <fit-dof>
       <sigma />
       <epsilon />
     </fit-dof>
   </lennard-jones>



.. rubric:: Elements and attributes

* ``<sigma>``: The initial value for the :math:`\sigma` parameter.
* ``<epsilon>``: The initial value for the :math:`\varepsilon` parameter.
* ``<cutoff>``: The cutoff radius.
