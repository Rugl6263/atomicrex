res
set term svg font 'Helvetica, 22' size 1200, 1000
set out 'parameters.svg'
set enc iso_8859_1

wdt = 0.42
wshift = wdt
hgt = 0.4
hshift = hgt - 0.1

set origin 0.1, 0.1
set size wdt + wshift, hgt + hshift
set multiplot

set size wdt, hgt

#--------------------------------------------------------
col_red    = '#D7191C'
col_orange = '#FDAE61'
col_yellow = '#FFFFBF'
col_blue1  = '#ABD9E9'
col_blue2  = '#2C7BB6'
col1 = '#d7191c'
col2 = '#fdae61'
col3 = '#abd9e9'
col4 = '#2c7bb6'
col5 = '#31a354'
col6 = '#a1d99b'
col7 = '#dd1c77'
col8 = '#c994c7'
#------------
set style incr user
LW=3 ; PS = 1.8
LS=0
LS=LS+1 ; set style line LS lt 1 lw LW lc rgb col_blue2
LS=LS+1 ; set style line LS lt 2 lw LW lc 0
#------------
set style incr user
LW=4
set style arr 11 lt 1 lw LW lc 0 filled head
set style arr 12 lt 1 lw LW lc 0 filled heads
set style arr 13 lt 1 lw 4 lc rgb '#bbbbbb' nohead
set style arr 14 lt 2 lw 4 lc 0 nohead

#--------------------------------------------------------
xc = 1e-3
set xla '{/=24 Trial step ({\327}10^3)}'
set xti 100
set key noautoti Left rev spac 1.4
set key right bot

#--------------------------------------------------------
set origin 0.1, 0.1 + hshift
set xla '&{/=24 Trial step ({\327}10^3)}'
set form x '&{%.0f}'
set yla '{/=24 Parameter A}'
set form y '%4.0f'
set yti 1000
p '<grep ^mc log.simulated-annealing' u (xc*$2):5 ev 10 w l

#--------------------------------------------------------
set origin 0.1 + wshift, 0.1 + hshift
set xla '&{/=24 Trial step ({\327}10^3)}'
set form x '&{%.0f}'
set yla '{/=24 Parameter lambda}'
set form y '%4.1f'
set yti 0.3
p '<grep ^mc log.simulated-annealing' u (xc*$2):6 ev 10 w l

#--------------------------------------------------------
set origin 0.1, 0.1
set xla '{/=24 Trial step ({\327}10^3)}'
set form x '{%.0f}'
set yla '{/=24 Parameter mu}'
set form y '%4.1f'
set yti 0.2
p '<grep ^mc log.simulated-annealing' u (xc*$2):7 ev 10 w l

#--------------------------------------------------------
set origin 0.1 + wshift, 0.1
set yla '{/=24 Parameter D}'
set form y '%4.0f'
set yti 5
p '<grep ^mc log.simulated-annealing' u (xc*$2):8 ev 10 w l
